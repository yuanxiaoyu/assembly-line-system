<?php
error_reporting(0);
include_once dirname(__FILE__) . '/Db.php';

class User {

    public $user_id;
    public $user_name;
    public $user_password;
    public $user_type_id;
    public $user_type_name;
    public $user_department_id = false;

    private function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:
                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {
        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_users,fac_user_type  where fac_users.user_type_id=fac_user_type.user_type_id and user_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->user_id = $row['user_id'];
        $this->user_name = $row['user_name'];
        $this->user_password = $row['user_password'];
        $this->user_type_name = $row['user_type_name'];
        $this->user_type_id = $row['user_type_id'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->user_id = $row['user_id'];
        $this->user_name = $row['user_name'];
        $this->user_password = $row['user_password'];
        $this->user_type_name = $row['user_type_name'];
        $this->user_type_id = $row['user_type_id'];
    }

    //根据id得到一个用户对象
    public static function get_user($id) {
        $id = intval($id);
        if (User::id_is_exist($id)) {
            return new User($id);
        } else {
            return false;
        }
    }

    //根据id判断用户是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_users where user_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        return $row["num"] >= 1;
    }

    //获取用户id
    public function get_id() {
        return $this->user_id;
    }

    //增加一个用户
    public static function add_a_user($c_name, $password, $user_type_id) {
        if (User::is_name_exist($c_name)) {
            return false;
        } else {
            $password = strtolower(hash('sha256', $password));
            $sql = "INSERT INTO fac_users (user_name,user_password,user_type_id)";
            $sql.="value ('$c_name','$password',$user_type_id)";
            return Db::get_instance()->query($sql);
        }
    }

    //判断数据库中是否已存在用户名
    public static function is_name_exist($name) {
        $sql = 'SELECT count(*) as num from fac_users where user_name=\'' . $name . '\'';

        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        $num = $row['num'];
        return $num > 0;
    }

    //分页获取用户列表
    public static function get_a_page_users($num_per_page = 20, $page_index = 0) {
        //page_index代表页的下标  从0开始
        if ($num_per_page == 0) {
            $limit = '';
        } else {
            $num1 = $page_index * $num_per_page;
            $limit = "LIMIT $num1,$num_per_page";
        }
        $sql = "SELECT
	fu.*,fut.user_type_name,fd.department_name
FROM
	fac_users fu
LEFT JOIN `fac_user_type` fut ON fut.user_type_id = fu.user_type_id
LEFT JOIN `fac_user_department` fud on fud.user_id = fu.user_id
LEFT JOIN `fac_department` fd on fd.department_id = fud.department_id
GROUP BY fu.user_id ORDER BY user_type_id ASC $limit;";
        $ret = Db::get_instance()->query($sql);
        foreach ($ret as $index => $r) {
            if ($r['user_type_id'] >= $_COOKIE['usertype']) {
                unset($ret[$index]);
            }
        }
        return $ret;
    }

    //注册一个用户,
    public static function reg($name, $password) {
        return User::add_a_user($name, $password, 5);
    }

    //删除一个用户
    public static function unreg($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_users where user_id=' . $id; //外键，约束删除
        Db::get_instance()->query('DELETE from `fac_user_department` where user_id=' . $id);
        return Db::get_instance()->query($sql);
    }

    //修改内容后更新一个用户
    public function update() {
        $this->user_password = strtolower(hash('sha256', $this->user_password));
        $sql = 'update fac_users set user_password=\'' . $this->user_password . '\',';
        $sql.='user_name=\'' . $this->user_name . '\',';
        $sql.='user_type_id=' . $this->user_type_id;
        $sql.=' where user_id=' . $this->user_id;
        if ($this->user_department_id != false && $this->user_department_id != '') {
            $ret1 = Db::get_instance()->query("UPDATE `fac_user_department` SET `department_id` = $this->user_department_id WHERE `user_id` = $this->user_id;");
        }
        $ret = Db::get_instance()->query($sql);
        return $ret || $ret1;
    }

    //用户登录 ,传入$_SESSION,把登录成功后的对象保存到$_SESSION 中
    public static function login($name, $password) {
        $name = addslashes($name);
        $password = strtolower(hash('sha256', $password));
        $sql = "SELECT * from fac_users where user_name='$name' and user_password='$password'";
        $rs = Db::get_instance()->query($sql);
        if (isset($rs[0])) {
            $row = $rs[0];
            # var_dump($row);
            if ($row['user_name'] == $name) {
                $user = new User($row['user_id']);
                return $user;
            }
        }
        return false;
    }

    //判断用户是否登录 ,传入Session
    public static function is_user_login(&$session) {
        if (isset($sesstion['User'])) {
            $User = $session['User'];
            return $User->user_id != -1;
        }
        return false;
    }

    //用户注销,传入Session
    public static function un_log(&$session) {
        if (isset($sesstion['User'])) {
            $User = $session['User'];
            return $User->user_id = -1;
        }
        return false;
    }

    //验证权限
}
