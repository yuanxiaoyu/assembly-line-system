<?php

include_once dirname(__FILE__) . '/Db.php';

class Material {

    private $material_id;
    public $material_name;
    public $type;
    public $number;
    public $remark;
    public $material_code;

    private function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {//构建函数在new的时候自动运行
        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_material_mess where material_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->material_id = $row['material_id'];
        $this->material_name = $row['material_name'];
        $this->type = $row['type'];
        $this->number = $row['number'];
        $this->material_code = $row['material_code'];
        $this->remark = $row['remark'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->material_id = $row['material_id'];
        $this->material_code = $row['material_code'];
        $this->material_name = $row['material_name'];
        $this->type = $row['type'];
        $this->number = $row['number'];
        $this->material_code = $row['material_code'];
    }

    //根据id得到一个材料对象
    public static function get_Material($id) {
        $id = intval($id);
        if (Material::id_is_exist($id))
            return new Material($id);
        else
            return false;
    }

    //根据id判断材料是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_material_mess where material_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        return $row["num"] > 0;
    }

    //获取材料id
    public function get_id() {
        return $this->product_id;
    }

    //增加一个材料
    public static function add_a_material($name, $type, $number, $material_code, $remark) {
        if (Material::is_name_exist($name)) {
            return false;
        }
        $sql = "INSERT INTO fac_material_mess (material_name,material_type,number,material_code,remark)";
        $sql.="value ('$name','$type',$number,'$material_code','$remark')";
        return Db::get_instance()->query($sql);
    }

    //判断数据库中是否已存在材料名
    public static function is_name_exist($name) {
        $sql = 'SELECT count(*) as num from fac_material_mess where material_name=\'' . $name . '\'';

        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        $num = $row['num'];
        return $num > 0;
    }

    //分页获取材料列表
    public static function getMaterialsList($num_per_page = 20, $page_index = 0) {
        //page_index代表页的下标  从0开始
        $num1 = $page_index * $num_per_page;
        if ($num_per_page == 0) {
            $limit = '';
        } else {
            $limit = "limit $num1,$num_per_page";
        }
        return Db::get_instance()->query("SELECT * from fac_material_mess WHERE hidden='no'  $limit;");
    }

    //删除一个材料
    public static function delete_a_material($id) {
        $id = intval($id);
        return Util::dbDelete('fac_material_mess', 'material_id', $id);
    }

    //修改内容后更新一个材料
    public function update() {
        $sql = 'update fac_material_mess set material_name=\'' . $this->material_name . '\',';
        $sql.='material_type=\'' . $this->type . '\',';
        $sql.='`material_code`=\'' . $this->material_code . '\',';
        $sql.='number=' . $this->number;
        $sql.=',`remark`=\'' . $this->remark . '\'';
        $sql.=' where material_id=' . $this->material_id;
        return Db::get_instance()->query($sql);
    }

    //原料退货
    public function take_back($num) {
        $sql = "UPDATE fac_material_mess SET number=number-$num WHERE material_id=" . $this->material_id;
        return Db::get_instance()->query($sql);
    }
    
    /**
     * 获取原料总数
     * @return type
     */
    public static function getMaterialCount(){
        $sql = "SELECT SUM(number) AS count FROM `fac_material_mess` WHERE `hidden` = 'no';";
        $res = Db::get_instance()->query($sql);
        return $res[0]['count'];
    }

}
