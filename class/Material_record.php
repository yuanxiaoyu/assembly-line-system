<?php

include_once dirname(__FILE__) . '/Db.php';

class Material_record {

    private $trade_id;
    public $material_id;
    public $price;
    public $trade_date;
    public $trade_number;

    private function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    //根据id构造函数
    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_material_record where trade_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->trade_id = $row['trade_id'];
        $this->material_id = $row['material_id'];
        $this->trade_number = $row['trade_number'];
        $this->trade_date = $row['trade_date'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->trade_id = $row['trade_id'];
        $this->material_id = $row['material_id'];
        $this->trade_number = $row['trade_number'];
        $this->trade_date = $row['trade_date'];
    }

    //根据id得到一个材料交易记录对象
    public static function get_Material_record_id($id) {

        $id = intval($id);

        if (Material_record::id_is_exist($id)) {

            return new Material_record($id);
        } else
            return false;
    }

    //根据id判断产品材料记录是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_material_record where trade_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        if ($row["num"] < 0) {
            return false;
        } else {
            return true;
        }
    }

    //获取材料交易记录id
    public function get_id() {
        return $this->trade_id;
    }

    //增加一条材料交易记录
    public static function add_a_record($material_id, $price, $trade_number) {
        $sql = "INSERT INTO fac_material_record(material_id,price,trade_date,trade_number)";
        $sql.="value ($material_id,$price,now(),$trade_number)";
         Db::get_instance()->query($sql);
        $sql1="UPDATE fac_material_mess SET number = number +  $trade_number  WHERE material_id=$material_id; ";
        $rs = Db::get_instance()->query($sql1);
        if ($rs) {
            return true;
        } else
            return false;
    }

    //分页获取产品交易记录列表
    public static function get_a_page_record($num_per_page, $page_index) {
        //page_index代表页的下标  从0开始
        if($num_per_page == 0){
            $limit = '';
        } else {
            $num1 = $page_index * $num_per_page;
            $limit = "limit $num1,$num_per_page;";
        }

        $sql = "SELECT * from fac_material_record t1 left join fac_material_mess t2 on t1.material_id=t2.material_id $limit ";
        $rs = Db::get_instance()->query($sql);

        return $rs;
    }

    //删除一条产品交易记录
    public static function del_a_record($id) {

        $id = intval($id);
        $sql = 'DELETE from fac_material_record where trade_id=' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //修改后保存记录
    public function update() {

        $sql = 'UPDATE fac_material_record set ';
        $sql.='material_id=\'' . $this->material_id . '\',';
        $sql.='price=\'' . $this->price . '\',';
        $sql.='trade_number=\'' . $this->trade_number . '\'';
        $sql.= ' where trade_id=' . $this->trade_id;

        $rs = Db::get_instance()->query($sql);
        if (!$rs)
            return false;
        else
            return true;
    }

}
