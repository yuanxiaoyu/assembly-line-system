<?php

include_once dirname(__FILE__) . '/Db.php';

class Customer {

    public $customer_id;  //客户id
    public $customer_name; //客户名称
    public $tel;   //客户电话
    public $qq; //客户qq
    public $e_mail; //客户邮件
    public $address;  //客户地址
    public $group_id;  //分组id
    public $remark;   //备注

    //构造函数

    public function __construct($id) {
        $db = Db::get_instance();
        $rs = $db->query("SELECT * from fac_customer,fac_group where fac_customer.group_id = fac_group.group_id and customer_id=" . $id);
        $row = $rs[0];
        $this->customer_id = $row["customer_id"];
        $this->customer_name = $row["customer_name"];
        $this->tel = $row["tel"];
        $this->qq = $row["qq"];
        $this->e_mail = $row["e_mail"];
        $this->address = $row["address"];
        $this->group_id = $row["group_id"];
        $this->remark = $row["remark"];
    }

    //生产一个客户对象
    public static function get_Customer($id) {
        $id = intval($id);
        if (Customer::id_is_exist($id)) {
            return new Customer($id);
        } else {
            return false;
        }
    }

    //根据id判断客户是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_customer where customer_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        return $rs[0]["num"] > 0;
    }

    //获取客户id
    public function get_id() {
        return $this->customer_id;
    }
    
    /**
     * 添加客户
     * @param type $c_name
     * @param type $c_tel
     * @param type $qq
     * @param type $e_mail
     * @param type $c_addr
     * @param type $c_remark
     * @param type $c_gid = 1 默认分组
     * @return boolean
     */
    public static function add_a_customer($c_name, $c_tel = '', $qq = '', $e_mail = '', $c_addr = '', $c_remark = '', $c_gid = 1) {
        $e_mail = strtolower($e_mail);
        //静态函数,调用方法，Customer.add_a_customer(名字，电话,地址，组id,备注)
        if (Customer::is_name_exist($c_name))
            return false;
        $sql = "INSERT INTO fac_customer (customer_name,tel,qq,e_mail,address,remark,group_id)";
        $sql.="value ('$c_name','$c_tel','$qq','$e_mail','$c_addr','$c_remark',$c_gid)";
        return Db::get_instance()->query($sql);
    }

    //判断数据库中是否已存在客户名
    public static function is_name_exist($name) {
        $sql = 'SELECT count(*) as num from fac_customer where customer_name=\'' . $name . '\'';
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        $num = $row['num'];
        return $num > 0;
    }

    //根据id获取客户列表
    public static function get_group_customer($id) {
        $id = intval($id);
        $sql = 'SELECT * from fac_customer where `hidden` = \'no\' AND group_id=' . $id;
        return Db::get_instance()->query($sql);
    }

    //修改后保存用户资料
    public function update() {
        $this->e_mail = strtolower($this->e_mail);
        $sql = 'update fac_customer set customer_name=\'' . $this->customer_name . '\',';
        $sql.='tel=\'' . $this->tel . '\',';
        $sql.='qq=\'' . $this->qq . '\',';
        $sql.='e_mail=\'' . $this->e_mail . '\',';
        $sql.='address=\'' . $this->address . '\',';
        $sql.='remark=\'' . $this->remark . '\',';
        $sql.='group_id=' . $this->group_id . ' where customer_id=' . $this->customer_id;
        return Db::get_instance()->query($sql);
    }

    //删除一个客户
    public static function delete_a_consumer($id) {
        return Util::dbDelete('fac_customer', 'customer_id', $id);
    }
    
    /**
     * 获取客户id by 客户名，如果没有则创建
     * @param type $name
     * @return type
     */
    public static function getByName($name){
        if(self::is_name_exist($name)){
            $ret1 = Db::get_instance()->query("SELECT `customer_id` FROM fac_customer WHERE customer_name = '$name';");
            return intval($ret1[0]['customer_id']);
        } else {
            return intval(self::add_a_customer($name));
        }
    }

    public function getAllCustomers() {
        return Db::get_instance()->query("SELECT * FROM fac_customer WHERE hidden='no'");
    }

}
