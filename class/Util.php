<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Util {

    public static function dbDelete($table, $dname, $val) {
        $delete = "delete from `$table` where `$dname` = '$val';";
        if (Db::get_instance()->query($delete)) {
            return true;
        } else {
            $delete = "UPDATE `$table` SET `hidden` = 'yes' where `$dname` = '$val';";
            if (Db::get_instance()->query($delete)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * 
     * @param type $timestamp
     * @return string
     */
    public static function timeConv($timestamp) {
        if ($timestamp == NULL) {
            return '';
        }
        $timestamp = strtotime($timestamp);
        $curTime = time();
        $curtimeArray = getdate($curTime);
        $timeArray = getDate($timestamp);
        if ($curtimeArray['year'] == $timeArray['year']) {
            $string = sprintf("%d月%d日 %02d:%02d", $timeArray['mon'], $timeArray['mday'], $timeArray['hours'], $timeArray['minutes']);
            return $string;
        }
        $string = sprintf("%d年%d月%d日 %02d:%02d", $timeArray['year'], $timeArray['mon'], $timeArray['mday'], $timeArray['hours'], $timeArray['minutes']);
        return $string;
    }

    public static function get($k) {
        return isset($_GET[$k]) ? $_GET[$k] : false;
    }

    public static function post($k) {
        return isset($_POST[$k]) ? $_POST[$k] : false;
    }

    /**
     * 计算生产订单进度百分比
     * @param type $id
     */
    public static function producePercent($id) {
        include_once dirname(__FILE__) . '/department.php';
        if (!is_numeric($id)) {
            return 0;
        }
        $Db = Db::get_instance();

        $productInfo = $Db->query("SELECT number FROM `fac_product_order` WHERE `product_order_id` = $id;");

        $department = new department();
        $view = $department->department_view_all();

        $number = intval($productInfo[0]['number']);

        $arr = 0;
        $inv = 0;

        foreach ($view as &$v) {
            $text = "
        SELECT
	po.product_order_code,pm.product_model,pm.product_code,pm.gongyi, (
		SELECT
			SUM(number)
		FROM
			fac_department_receive_record frr
		WHERE
			frr.department_id = fdr.department_id
		AND frr.order_id = fdr.order_id
	) as getX,
	SUM(fdr.number) as numberX
FROM
	fac_department_product_record fdr
LEFT JOIN fac_product_order po on po.product_order_id = fdr.order_id
LEFT JOIN fac_product_mess pm on pm.product_id = fdr.product_id
WHERE
	fdr.department_id = $v[department_id] AND fdr.order_id = $id
GROUP BY fdr.order_id,fdr.product_id
            ORDER BY
	numberX DESC";
            $ret = $Db->query($text);
            if (intval($v['is_first']) == 0 && $ret[0]['numberX'] > 0) {
                $arr+=($ret[0]['numberX'] / $ret[0]['getX']) * 100;
                $inv++;
            }
        }
        # echo $arr . ' ' . $inv . ' ';
        $arr = $arr / $inv;
        return round($arr, 2);
    }

}
