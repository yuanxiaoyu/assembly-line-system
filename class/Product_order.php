<?php

/*
 * 生产订单
 * 新建，删除，修改，生产订单
 *   $product_order_id;订单号
  $flow_id;流水线号
  $product_id;产品号
  $number;产品数量
  $create_date;下单时间
  $order_state;订单状态
  $finish_date;完成时间
 */

include_once dirname(__FILE__) . '/Product.php';
include_once dirname(__FILE__) . '/Product_record.php';
include_once dirname(__FILE__) . '/Product_order_item.php';
include_once dirname(__FILE__) . '/Material_record.php';

Class Product_order {

    private $product_order_id;
    public $flow_id;
    public $product_id;
    public $number;
    public $create_date;
    public $order_state;
    public $finish_date;

    public function __construct() {
        $args_num = func_num_args(); //获取参数个数
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    private function __construct1($id) {
        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_product_order where product_order_id=" . $id;
        //echo $sql;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->product_order_id = $row['product_order_id'];
        $this->flow_id = $row['flow_id'];
        $this->number = $row['number'];
        $this->product_id = $row['product_id'];
        $this->order_state = $row['order_state'];
        $this->create_date = $row['create_date'];
        $this->finish_date = $row['finish_date'];
    }

    //根据数据库查询结果的构造函数
    private function __construct2($row) {
        $this->product_order_id = $row['product_order_id'];
        $this->flow_id = $row['flow_id'];
        $this->number = $row['number'];
        $this->product_id = $row['product_id'];
        $this->order_state = $row['order_state'];
        $this->create_date = $row['create_date'];
        $this->finish_date = $row['finish_date'];
    }

    //根据id得到一个生产订单对象
    public static function get_product_order($id) {
        $id = intval($id);
        if (Product_order::id_is_exist($id))
            return new Product_order($id);
        else
            return false;
    }

    //根据id判断订单是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_product_order where product_order_id=" . $id;
        $rs = Db::get_instance()->query($sql);
        $row = $rs[0];
        return $row["num"] > 0;
    }

    //获取订单id
    public function get_id() {
        return $this->product_order_id;
    }

    //增加一个订单
    public static function add_a_order($product_id, $code, $number, $flow_id, $remark) {
        $sql = "INSERT INTO fac_product_order (product_id,product_order_code,number,flow_id,create_date,remark)";
        $sql.=" value ($product_id,'$code',$number,$flow_id,now(),'$remark')";
        return Db::get_instance()->query($sql);
//        $order_id = Db::get_instance()->query($sql);
//        
//        echo $sql;
//
//        $rs = Db::get_instance()->query("SELECT * FROM fac_flow_mess where flow_id = $flow_id ORDER BY sort ASC;");
//
//        $sql = "INSERT INTO fac_order_department_mess (product_order_id,`sort`,department_id,product_sum,taskstate,product_yes,product_no,product_wait,worktime) values";
//
//        foreach ($rs as $row) {
//            $department_id = $row['department_id'];
//            $sort = $row['sort'];
//            if ($sort == 1) {
//                $sql.="($order_id,$sort,$department_id,$number,'未开始',0,0,$number,0),";
//            } else {
//                $sql.="($order_id,$sort,$department_id,0,'未开始',0,0,0,0),";
//            }
//        }
//
//        $sql = substr($sql, 0, strlen($sql) - 1);
//
//        return Db::get_instance()->query($sql);
    }

    //保存订单条目
    public static function save_material_item($order_id, $materials) {
        $sql = "DELETE fac_product_order_item where product_order_id=" . $order_id;
        Db::get_instance()->query($sql);
        $sql = "INSERT INTO fac_product_order_item (material_id,number,product_order_id) values ";
        foreach ($materials as $material_item) {
            $material_id = $material_item['material_id'];
            $number = $material_item['number'];
            $sql.="($material_id,$number,$order_id),";
        }
        $sql = substr($sql, 0, strlen($sql) - 1);
        // echo $sql;
        return Db::get_instance()->query($sql) > 0;
    }

    //获取订单列表
    public static function get_orders($num_per_page, $page_index, $where = '') {
        $num1 = $page_index * $num_per_page;
        if ($num_per_page == 0) {
            $limit = '';
        } else {
            $limit = "limit $num1,$num_per_page";
        }
        $sql = "SELECT *,t1.number as pnumber,t1.order_state + 0 AS order_stateint,DATEDIFF(t1.finish_date,t1.create_date) AS wtime from fac_product_order t1 left join fac_flow t2 on t1.flow_id=t2.flow_id left join fac_product_mess t3 on t1.product_id=t3.product_id $where ORDER BY `create_date` DESC " . $limit;
        $rs = Db::get_instance()->query($sql);
        return $rs;
    }

    //修改后保存订单
    public function update() {
        $sql = 'UPDATE fac_product_order set product_id=' . $this->product_id . ',';
        $sql.='create_date=' . $this->create_date . ',';
        $sql.='order_state=\'' . $this->order_state . '\',';
        $sql.='finish_date=' . $this->finish_date . ' where product_order_id=' . $this->product_order_id;

        return Db::get_instance()->query($sql);
    }

    //删除一个订单
    public static function delete_a_product_order($id) {
        $id = intval($id);
        $sql = 'DELETE * from fac_product_order where product_order_id =' . $id;
        $sql1 = 'DELETE * from fac_product_order_item where product_order_id =' . $id;
        Db::get_instance()->query($sql1);
        return Db::get_instance()->query($sql);
    }

    //完成一个订单,传入变量
    public function finish($finish_number) {

        //修改订单状态
        $sql = $sql = "UPDATE fac_product_order set finish_date=now() ,";
        $sql.="order_state='已完成',finish_number=" . $finish_number;
        $sql.= ' where product_order_id=' . $this->product_order_id;

        if (Db::get_instance()->query($sql) <= 0) {
            return false;
        }

//        //增加产品到仓库
//        $product = Product::get_Product($this->product_id);
//        if (!$product) {
//            return false;
//        }
//
//        $product->number+=$finish_number;
//
//        if (!$product->update()) {
//            return false;
//        }

        return true;
    }

    public function start() {//生成oreder_department_mess表
        $this->order_state = '进行中';
        $this->update();
        $sql = "SELECT * FROM fac_flow_mess where flow_id=" . $this->flow_id;

        $rs = Db::get_instance()->query($sql);

        $sql = "INSERT INTO fac_order_department_mess (product_order_id,department_id,prouct_sum,taskstate,product_yes,product_no,product_wait,worktime) values";
        foreach ($rs as $row) {
            $department_id = $row['department_id'];
            $sql.="($this->product_order_id,$department_id,0,'未开始',0,0,0,0),";
        }

        $sql = substr($sql, 0, strlen($sql) - 1);

        Db::get_instance()->query($sql);

        return true;
    }

    /**
     * 获取订单数据
     * @param type $id
     * @return type
     */
    public static function getOrderData($id) {
        return Db::get_instance()->query("SELECT *,t1.number as pnumber from fac_product_order t1 "
                        . "left join fac_flow t2 on t1.flow_id=t2.flow_id "
                        . "left join fac_product_mess t3 on t1.product_id = t3.product_id "
                        . "WHERE `product_order_id` = $id;");
    }

    /**
     * 获取车间-订单生产进度
     */
    public static function getOrderMessPercent($department_id, $orderid) {
        $ret = Db::get_instance()->query("SELECT coalesce(CEIL(((t3.product_yes + t3.product_no) / t3.product_sum) * 100),0) AS percent"
                . " FROM fac_order_department_mess t3 WHERE t3.product_order_id = $orderid AND department_id = $department_id;");
        // var_dump($ret);
        return $ret[0]['percent'];
    }

    /**
     * 获取生产订单材料列表
     * @param type $id
     * @return type
     */
    public static function getOrderMeterialList($id) {
        $sql = "SELECT fac_product_order_item.*,fac_material_mess.material_name,fac_material_mess.material_type FROM fac_product_order_item LEFT JOIN fac_material_mess on fac_material_mess.material_id = fac_product_order_item.material_id WHERE fac_product_order_item.product_order_id = $id;";
        return Db::get_instance()->query($sql);
    }

    /**
     * 获取车间生产情况
     * @param type $id
     * @return type
     */
    public static function getOrderFlowsData($id) {
        return Db::get_instance()->query("SELECT
	fd.department_id,
	fd.department_name,
	fom.sort,
	fom.taskstate,
	fom.taskstate + 0 AS statusint,
	fom.product_yes,
	fom.product_no,
	fom.product_sum,
	fom.product_wait,
	fom.worktime
FROM
	`fac_order_department_mess` fom
LEFT JOIN `fac_product_order` fo ON fo.product_order_id = fom.product_order_id
LEFT JOIN `fac_department` fd ON fd.department_id = fom.department_id
WHERE
	fom.product_order_id = $id
GROUP BY
	fom.sort
ORDER BY
	fom.sort ASC");
    }

    /**
     * 取消生产订单
     * @param type $orderId
     * @return 0 1 -1
     */
    public static function delete($orderId) {
        $db = Db::get_instance();
        $res = Db::get_instance()->query("DELETE FROM `fac_order_department_mess` WHERE `product_order_id` = $orderId;");
        $res1 = Db::get_instance()->query("DELETE FROM `fac_product_order_item` WHERE `product_order_id` = $orderId;");
        $res2 = Db::get_instance()->query("DELETE FROM `fac_product_order` WHERE `product_order_id` = $orderId;");
        return $res || $res1 || $res2;
    }

}
