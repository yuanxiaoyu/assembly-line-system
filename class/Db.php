<?php

include_once dirname(__FILE__) . '/Util.php';
//error_reporting(0);

/*
 * Copyright (C) 2014 koodo@qq.com.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

/**
 * Dao模块，采用PDO
 */
class Db {

    // dsn
    private $dsn = "";
    // db
    private $db;
    private static $instance = false;
    // memcached
    private $memcached = false;

    public static function get_instance() {
        if (!self::$instance) {
            $instance = new Db();
        }
        return $instance;
    }

    /**
     * 
     * @global type $config
     */
    public function __construct() {
        if (class_exists('PDO')) {
            global $config;
            include dirname(__FILE__) . '/../config.php'; //dirname(__FILE__)取得当前的目录名
            if($config->memcached['on']){
                $this->initMemcache($config);
            }
            $this->dsn = sprintf("mysql:host=%s;dbname=%s", $config->db['host'], $config->db['db']);
            $this->db = new PDO($this->dsn, $config->db['user'], $config->db['pass']);
            $this->db->exec("SET NAMES 'UTF8'");
        } else {
            die('请开启PDO和POD_MYSQL模块');
        }
    }

    /**
     * @param type $statement
     * @return type
     */
    public function query($statement, $fetchStyle = PDO::FETCH_ASSOC) {
        if (isset($_GET['desql']))
            echo $statement . '____';
        if (preg_match("/INSERT/is", $statement)) {
            // INSERT
            $this->db->exec($statement);
            $result = $this->db->lastInsertId();
        } else if (preg_match("/UPDATE|DELETE/is", $statement)) {
            // UPDATE|DELETE=
            $result = $this->db->exec($statement);
        } else {
            // memcached
            if ($this->memcached) {
                global $config;
                $sHash = md5($statement . $config->memcached['monk']);
                $mca = $this->memcached->get($sHash);
                if ($mca) {
                    return $mca;
                } else {
                    $query = $this->db->prepare($statement);
                    $query->execute();
                    $result = $query->fetchAll($fetchStyle);
                    $this->memcached->set($sHash, $result, MEMCACHE_COMPRESSED, 50);
                }
            }
            // SELECT
            $query = $this->db->prepare($statement);
            $query->execute();
            $result = $query->fetchAll($fetchStyle);
        }
        return $result;
    }

    /**
     * 获取一个结果
     * @param type $statement
     * @return type
     */
    public function getOne($statement) {
        $res = $this->query($statement);
        return current($res[0]);
    }

    /**
     * 
     * @param type $statement
     * @return type
     */
    public function exec($statement) {
        return $this->db->exec($statement);
    }

    private function initMemcache($config) {
        if (class_exists('Memcache')) {
            $memcache = new Memcache;
            if ($memcache->connect($config->memcached['host'], $config->memcached['port'])) {
                $this->memcached = $memcache;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
