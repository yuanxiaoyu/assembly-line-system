<?php

if(!is_numeric($_COOKIE['usertype']) || !is_numeric($_COOKIE['userid'])){
    header('Location:/');
    exit(0);
}
header("Pragma:no-cache\r\n");
header("Cache-Control:no-cache\r\n");
header('Content-Type:text/html');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('PREFIX', 'fac_');

include_once dirname(__FILE__) . '/../class/Db.php';
include_once dirname(__FILE__) . '/../class/Smarty/Smarty.class.php';
include_once dirname(__FILE__) . '/Users.php';

$GLOBALS['user'] = User::get_user($_COOKIE['userid']);

/**
 * 自动加载
 * @param type $className
 */
function __autoload($className) {
    $_path = dirname(__FILE__) . "/class/$className.php";
    if (is_file($_path)) {
        include_once $_path;
    } else {
        die('找不到类' . $className);
    }
}

$Smarty = new Smarty();

// 打开smarty文件缓存
// $Smarty->caching = true;

$Smarty->setTemplateDir(dirname(__FILE__) . '/../pages/template/');
$Smarty->setCompileDir(dirname(__FILE__) . '/../pages/template_c/');
$Smarty->setCacheDir(dirname(__FILE__) . '/../pages/template_cache/');

$Smarty->assign('vers', date('Y-m-d'));