<?php

include_once dirname(__FILE__) . '/Db.php';

class Product_order_item {

    private $product_order_item_id;
    public $material_id;
    public $number;
    public $product_order_id;

    public function __construct() {
        $args_num = func_num_args(); //获取参数个数���
        $args = func_get_args(); //获取参数列表

        switch ($args_num) {
            case 1:
                $this->__construct1($args[0]);
                break;
            case 2:
                $this->__construct2($args[1]);
                break;
            default:

                break;
        }
    }

    private function __construct1($id) {

        $db = Db::get_instance();
        $sql = "SELECT * FROM  fac_product_order_item where product_order_item_id=" . $id;
        $rs = $db->query($sql);
        $row = $rs[0];
        $this->product_order_item_id = $row['product_order_item_id'];
        $this->material_id = $row['material_id'];
        $this->number = $row['number'];
        $this->product_order_id = $row['product_order_id'];
    }

    //构造函数
    private function __construct2($row) {
        $this->product_order_item_id = $row['product_order_item_id'];
        $this->material_id = $row['material_id'];
        $this->number = $row['number'];
        $this->product_order_id = $row['product_order_id'];
    }

    //通过id，新建一个对象
    public static function get_product_order_item($id) {
        $id = intval($id);
        if (Product_order_item::id_is_exist($id))
            return new Product_order_item($id);
        else
            return false;
    }

    //判断ID是否存在
    public static function id_is_exist($id) {
        $id = intval($id);
        $sql = "SELECT count(*) as num from fac_product_order_item where product_order_item_id=" . $id;
        $rs = Db::get_instance()->query($sql);

        $row = $rs[0];
        if ($row["num"] < 1)
            return false;
        else
            return true;
    }

    //获取一个id
    public function get_id() {
        return $this->product_order_item_id;
    }

    //添加
    public static function add_a_product_order_item($c_mid, $c_number, $c_product_order_id) {
        $sql = "INSERT INTO fac_product_order_item (material_id,number,product_order_id)";
        $sql.="value ($c_mid,$c_number,$c_product_order_id)";
//        echo $sql;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

    //删除
    public static function delete_a_product_order_item($id) {
        $id = intval($id);
        $sql = 'DELETE from fac_product_order_item where product_order_item_id =' . $id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

//更新����
    public function update() {
        $sql = 'update fac_product_order_item set material_id =' . $this->material_id . ',';
        $sql.='product_order_id=' . $this->product_order_id . ',';
        $sql.='number=' . $this->number;
        $sql.=' where product_order_item_id=' . $this->product_order_item_id;
        $rs = Db::get_instance()->query($sql);
        if ($rs)
            return true;
        else
            return false;
    }

//分页查询
    public static function get_a_page_item($id, $orderby = 'number') {
        $sql = "SELECT  * from fac_product_order_item where product_order_id=" . $id . " order by $orderby";
        $rs = Db::get_instance()->query($sql);
        return $rs;
    }

}

?>