<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductStats
 *
 * @author Administrator
 */
class ProductStats extends Stats {

    private $Db = false;
    private $QueryMonth = false;

    public function __construct($QueryMonth) {
        $this->Db = Db::get_instance();
        $this->QueryMonth = $QueryMonth;
    }

    // 获取销售数据
    public function getProductSData() {
        $ret = $this->Db->query("select `product_model`,`number`,(`number` / (SELECT SUM(`number`) FROM `fac_product_mess` WHERE `hidden` = 'no'))*100 AS percent from `fac_product_mess` WHERE `hidden` = 'no' GROUP BY `product_id` ORDER BY number DESC LIMIT 15");
        $r1 = array();
        $r2 = array();
        $r3 = array();
        foreach ($ret as $r) {
            $r1[] = $r['product_model'];
            $r2[] = $r['number'];
            $r3[] = array($r['product_model'], floatval($r['percent']));
        }
        $this->echoJson(array(
            'a' => $r1,
            'b' => $r2,
            'c' => $r3
        ));
    }

    /**
     * 获取产量数据
     */
    public function getProduceData() {
        $ret = $this->Db->query("select DATE_FORMAT(finish_date,'%m-%d') AS m,SUM(finish_number) AS amount,SUM(DATEDIFF(finish_date,create_date)) AS wtime from fac_product_order WHERE DATE_FORMAT(finish_date,'%Y-%m') = '$this->QueryMonth'  AND order_state = '已完成' GROUP BY `m`;");
        # echo "select DATE_FORMAT(finish_date,'%m-%d') AS m,(finish_date - create_date) / 3600 AS wtime from fac_product_order WHERE DATE_FORMAT(finish_date,'%Y-%m') = '$this->QueryMonth'  AND order_state = '已完成' GROUP BY `m`;";
        $r1 = array();
        $r2 = array();
        $r3 = array();
        foreach ($ret as $r) {
            $r1[] = $r['m'];
            $r2[] = (int) $r['amount'];
            // $r3[] = (int) $r['wtime'];
        }
        $this->echoJson(array(
            'a' => $r1,
            'b' => $r2
            // 'c' => $r3
        ));
    }

    // 获取产量产品占比
    public function getProductDRate() {
        $ret = $this->Db->query("SELECT pm.product_model,pd.finish_number / (SELECT SUM(`fac_product_order`.finish_number) FROM `fac_product_order` WHERE DATE_FORMAT(`fac_product_order`.create_date,'%Y-%m') = '$this->QueryMonth') * 100 AS percent from `fac_product_order` pd LEFT JOIN fac_product_mess pm on pm.product_id = pd.product_id
WHERE DATE_FORMAT(pd.create_date,'%Y-%m') = '$this->QueryMonth' AND `pd`.order_state = '已完成'
GROUP BY pd.product_id");
        $r1 = array();
        foreach ($ret as $r) {
            $r1[] = array($r['product_model'] , (float) $r['percent']);
        }
        $this->echoJson($r1);
    }

}
