<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Stats {

    /**
     * decode unicode
     * @param type $str
     * @return type
     */
    function unIescape($str) {
        return str_replace('\/', '/', preg_replace("#\\\u([0-9a-f]{4})#ie", "iconv('UCS-2', 'UTF-8', pack('H4', '\\1'))", $str));
    }
    
    /**
     * echo json
     * @param type $arr
     */
    function echoJson($arr) {
        print_r(json_encode($arr));
    }

}
