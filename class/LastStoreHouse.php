<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LastStoreHouse {

    public function getProductList() {
        $SQL = "SELECT
	product_code,finish_number as number,product_model,gongyi
FROM
	fac_department_storehouse st
LEFT JOIN fac_department fd ON fd.department_id = st.department_id
left join fac_product_mess pm on pm.product_id = st.product_id
WHERE
	fd.is_final = 1";
        return Db::get_instance()->query($SQL);
    }

    public function getProductCount() {
        $SQL = "SELECT
	SUM(finish_number) AS count
FROM
	fac_department_storehouse st
LEFT JOIN fac_department fd ON fd.department_id = st.department_id
left join fac_product_mess pm on pm.product_id = st.product_id
WHERE
	fd.is_final = 1";
        $r = Db::get_instance()->query($SQL);
        return $r[0]['count'];
    }

}
