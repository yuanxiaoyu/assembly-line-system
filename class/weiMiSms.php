<?php

define('WEIMIUID','SqTOhc86HEuU');
define('WEIMIKEY','z7afpr9n');

/*
 * Copyright (C) 2014 Administrator.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

//    代码	结果描述	        解决方案
//    0	        短信发送成功	非常好，继续发送下一条
//    -1	参数不正确	确认是否遗漏必须传入的参数，以及格式和编码是否正确
//    -2	非法账号	        UID及密码不正确，或者账号被暂时（或永久）关闭，请联系客服
//    -3	IP鉴权失败	在合法的IP上调用接口，或将该IP添加为鉴权IP
//    -4	账号余额不足	请在线充值
//    -5	下发失败	        系统异常造成，请稍后重试
//    -6	短信内容含有非法关键字	请根据返回结果中提示的非法关键字，重新调整短信内容
//    -7	同一个号码、同一段短信内容，在同一小时内重复下发	为了避免对手机用户的骚扰，请避免类似下发，或联系客服
//    -8	拓展特服号码不正确	重新确认号码有效性，并重新设置
//    -9	非法子账号	子UID不正确，或者账号被暂时（或永久）关闭，请联系客服
//    -10	定时计划时间不正确	重新确认定时计划时间格式及有效性，并重新设置

class weiMiSms {

    /**
     * @param type $phone
     * @param type $cid 模板id
     * @param type $pm  模板参数
     * @return <array>
     */
    public function sendSmsTemplate($phone, $cid, $pm) {
        // pack params
        $phone = str_replace(' ', '', $phone);
        $paramp = "";
        if (is_array($pm)) {
            foreach ($pm as $index => $p) {
                $paramp .= ("&p" . ($index + 1) . "=$p");
            }
        } else {
            $paramp = '&p1=' . $pm;
        }
        // pack params end
        $curl = curl_init();
        $url = "http://api.weimi.cc/2/sms/send.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json&mob=$phone&cid=$cid$paramp";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode($res, true);
    }

    /**
     * 发送短信，非模板
     * @param type $phone
     * @param type $cont
     * @return <array>
     */
    public function sendSms($phone, $cont) {
        $phone = str_replace(' ', '', $phone);
        $curl = curl_init();
        $url = "http://api.weimi.cc/2/sms/send.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json&mob=$phone&con=$cont";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode($res, true);
    }

    /**
     * 获取短信使用情况
     * @return <array>
     */
    public function getSmsUseAge() {
        $curl = curl_init();
        $url = "http://api.weimi.cc/2/sms/send.html?uid=" . WEIMIUID . "&pas=" . WEIMIKEY . "&type=json";
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        curl_close($curl);
        return json_decode(str_replace('""', '","', $res), true);
    }

}
