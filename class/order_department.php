<?php

include_once dirname(__FILE__) . '/Db.php';

class order_department {

    public $id; //自动生产
    public $product_order_id; //订单ID
    public $department_id; //部门ID
    public $product_sum; //生产数量
    public $product_yes; //合格品
    public $product_no; //不合格品
    public $product_wait; //待加工成品
    public $worktime; //工作时间
    public $taskstate; //订单状态

    public function __construct() {
        $db = Db::get_instance();
    }

    /**
     * 根据车间id，订单id查看订单流程上个车间id
     * @param type $department_id
     * @param type $order_id
     * return 车间id（int类型 ）
     * return 0 表示在流程中为第一个车间
     */
    private static function GetSort($department_id, $order_id) {
        $sql = "SELECT sort FROM fac_flow_mess 
                    WHERE flow_id = 
                        (SELECT flow_id FROM fac_product_order WHERE product_order_id=$order_id)
				AND department_id=$department_id";
        $sort = Db::get_instance()->query($sql); //获取sort
        return $sort = $sort[0]['sort'];
    }

    public static function update($cid, $sort, $product_yes, $product_no, $product_wait, $worktime) {

        if ($product_yes >= 0 && $product_no >= 0 && $product_wait >= 0) {

            // 获取对应订单id
            $pdata = Db::get_instance()->query("SELECT `product_order_id` FROM `fac_order_department_mess` WHERE `id` = $cid");
            if ($pdata) {
                $order_id = intval($pdata[0]['product_order_id']);
            } else {
                return false;
            }

            // 生产状态
            Db::get_instance()->query("UPDATE `fac_product_order` SET `order_state` = '生产中' WHERE product_order_id = $order_id;");
            Db::get_instance()->query("UPDATE `fac_order_department_mess` SET `taskstate` = '生产中' WHERE `id` = $cid;");

            // 更新当前车间数据
            $sql = "UPDATE fac_order_department_mess SET product_yes = $product_yes,product_no=$product_no,product_wait=$product_wait,worktime=$worktime";
            $sql.=" WHERE `id` = $cid;";
            $ret = Db::get_instance()->query($sql);

            // 数据传递到下一个车间
            $sort++;
            //print_r($sort);
            $sql1 = "UPDATE fac_order_department_mess SET product_sum = $product_yes,product_wait = $product_yes";
            $sql1.=" WHERE product_order_id=$order_id AND sort = $sort;";
            $ret2 = Db::get_instance()->query($sql1);

            return $ret || $ret2;
        } else {
            return 0;
        }
    }

    /**
     * 更新生产订单状态
     * @param type $department_id
     * @param type $order_id
     * @param type $taskstate
     * @return type
     */
    public static function state_update($cid, $taskstate, $worktime) {
        if ($cid > 0) {
            // 更新订单状态
            if ($worktime > 0) {
                $sql = "UPDATE `fac_order_department_mess` SET taskstate='$taskstate',`worktime` = $worktime WHERE `id` = $cid;";
            } else {
                $sql = "UPDATE `fac_order_department_mess` SET taskstate='$taskstate' WHERE `id` = $cid;";
            }
            $ret1 = Db::get_instance()->query($sql);
            return $ret1 !== false ? 1 : 0;
        } else {
            return $cid;
        }
    }

    /**
     * 完成生产订单
     * @param type $cid
     */
    public static function finish($cid, $order_id) {
        $pid = Db::get_instance()->query("SELECT `product_id` FROM `fac_product_order` WHERE `product_order_id` = $order_id;");
        $pid = $pid[0]['product_id'];
        // 获取最终product_yes
        $productYes = Db::get_instance()->query("SELECT `product_yes` FROM `fac_order_department_mess` WHERE `id` = $cid;");
        $productYes = $productYes[0]['product_yes'];
        // 更新库存数据 订单状态
        $ret2 = Db::get_instance()->query("UPDATE `fac_product_mess` SET number = number + $productYes WHERE `product_id` = $pid;");
        $ret3 = Db::get_instance()->query("UPDATE `fac_product_order` SET `order_state` = '已完成',`finish_number` = $productYes WHERE product_order_id = $order_id;");
        return $ret2 || $ret3;
    }

    /**
     * 
     * @param type $department_id
     * @return type
     */
    public static function view($department_id) {
        $sql = "SELECT
	t1.product_order_id,
	t1.taskstate,
	t1.worktime,
        t1.sort,
        t1.product_yes,
        t1.product_no,
        t1.product_sum,
        t1.product_wait,
        t1.id,
	t2.product_id,
        t3.*,
	coalesce(CEIL(((t1.product_yes + t1.product_no) / t1.product_sum) * 100),0) AS percent
FROM
	fac_order_department_mess t1
LEFT JOIN fac_product_order t2 ON t1.product_order_id = t2.product_order_id
LEFT JOIN fac_product_mess t3 ON t2.product_id = t3.product_id
WHERE
	department_id = $department_id AND taskstate <> '已完成'";
        return Db::get_instance()->query($sql);
    }

    public static function view_department_order($department_id, $order_id) {
        $sql = "SELECT
         t1.product_order_id,
         t1.taskstate,
         t1.worktime,
         t2.product_id
 FROM
         fac_order_department_mess t1
 LEFT JOIN fac_product_order t2 ON t1.product_order_id = t2.product_order_id
 WHERE
         department_id =$department_id AND t1.product_order_id =$order_id";
        return Db::get_instance()->query($sql);
    }

    /**
     * 查询上个车间的工作状态
     * @param type 
     * @param type $order_id
     * @return type
     */
    public static function view_before_state($sort, $order_id) {
        $sort--;
        if ($sort > 0) {
            $sql = "SELECT fac_order_department_mess.*,fac_department.department_name FROM fac_order_department_mess LEFT JOIN `fac_department` ON fac_order_department_mess.department_id = `fac_department`.department_id WHERE product_order_id=$order_id  AND sort=$sort";
            #echo $sql;
            return Db::get_instance()->query($sql);
        } else {
            return 0;
        }
    }

    /**
     * 查看一个车间详情
     * @param type $cid
     * @return type
     */
    public static function view_one_department_order($cid) {
        $sql = "SELECT * FROM fac_order_department_mess WHERE id = $cid";
        return $taskstate = Db::get_instance()->query($sql);
    }

    /**
     * 呵呵
     */
    public static function getMaxSort($id) {
        $ret = Db::get_instance()->query("select MAX(sort) AS sort_max from fac_order_department_mess where fac_order_department_mess.product_order_id = $id");
        return $ret[0]['sort_max'];
    }

    /**
     * 暂停车间所有生产操作
     * @param type $department_id
     */
    public static function pauseWorkShop($department_id) {
        $sql = "UPDATE fac_order_department_mess SET taskstate='已暂停' WHERE `department_id` = $department_id;";
        return Db::get_instance()->query($sql);
    }

}
