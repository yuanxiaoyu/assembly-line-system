
var bodPadding = 35;

var currentPage = false;

var mainFrameNode = false;

// 计时器开始标记
var timerStarted = false;

var cookiePath = '/pages';

/**
 * Object extension
 * @param {type} o
 * @returns {F|Object.onew.F}
 */
Object.onew = function (o) {
    var F = function (o) {

    };
    F.prototype = o;
    return new F;
};

/**
 * loading animate
 * @type @exp;Object@call;onew
 */
var Loading = Object.onew({
    start: function (id) {
        Spinner.spin($(id).get(0));
    },
    finish: function () {
        Spinner.spin().stop();
    }
});

// jQuery onload
$(function () {
    window.onresize = resize;
    resize();

    $('.left_nav > .item').click(function () {
        $('.item.hover').removeClass('hover');
        $('.item_single.hover').removeClass('hover');
        $(this).toggleClass('hover');
        var subnav = $(this).parent().find('.subnav').eq(0);
        if (!subnav.hasClass('down')) {
            $('.subnav').each(function (i, node) {
                $(node).removeClass('down');
                $(node).slideUp(200);
            });
            subnav.addClass('down').slideDown(200);
        } else {
            subnav.addClass('down').slideDown(200);
        }
        $(this).parent().find('.subnav .item').eq(0).click();
    });

    $('.subnav > .item ,.left_nav > .item_single').click(function () {
        $('.item.hover').removeClass('hover');
        $('.item_single.hover').removeClass('hover');
        $(this).toggleClass('hover');
        //  $('#hello').hide();
        // mainFrameNode.show();
        switchPage($(this).attr('data-href'));
        $('#top').html($(this).html());
    });

    $('.topNavItem').click(function () {
        var href = $(this).attr('data-href')
        if (href) {
            switchPage(href);
        }
        href = null;
    });

    $('.left_nav > #caigou').click(function () {
        $('#caigou.hover').removeClass('hover');
        $(this).toggleClass('hover');
        $('#right_iframe').attr('src', '/pages/' + $(this).attr('data-href') + '.php?mod=hidden').show();
        $('#top').html($(this).html());
    });

    $('.left_nav  > #caigou').click();
    $('.left_nav > .subnav').find('.item').eq(0).click();
    $('.left_nav > .item').eq(0).addClass('hover');

    $('#department_home .button').click(function () {
        $('#department_home').hide();
        $('#department_next').show();
        resize();
    });

    $('#department_next_button').click(function () {
        $('#department_home').show();
        $('#department_next').hide();
        resize();
    });

    $('#timer-start').click(function () {
        if (!timerStarted) {
            Timer.start(timercallback);
            timerStarted = true;
        }
    });

    $('#timer-stop').click(function () {
        Timer.pause(timercallback);
        timerStarted = false;
    });

    // 登录按钮点击
    $('.login-gbtn').click(loginCheck);
    // 密码输入框回车
    $('#pd-form-password').keyup(function (e) {
        if (e.keyCode === 13) {
            loginCheck();
        }
    });

    if (parseInt($.cookie('usertype')) === 4) {
        bodPadding = 13;
        switchPage('workshop/index');
    }

});

function resize() {
    // 左导航
    $('#left_nav,#frame').height(document.documentElement.clientHeight - $('#topnav').height() - bodPadding);
    // main frame 垂直居中
    $('#wframe').css('margin-top', ($(window).height() - $('#wframe').height() - $('#topnav').height()) / 2); //厂长页面高度
    $('#login').css('margin-top', (document.documentElement.clientHeight - $('#login').height() - 70) / 2);
    // $('#login').width($(window).width() * 0.6);
    // 内容iframe高度 宽度
    $('#right_iframewap').height($('#right_iframewap').parent().height()).width($('#frame').width() - $('#left_nav').width() - 1);
    $('#right_iframe').height($('#right_iframe').parent().height()).width($('#right_iframewap').width());
    // 提示
    $('#__alert__').css('left', (document.documentElement.clientWidth - $('#__alert__').width()) / 2);
    // $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height() - 20) / 2);
}

//关闭fancybox之后的页面跳转函数
function close_switchPage(data) {
    switchPage(data);
}

//设置right_iframe的src函数
function switchPage(pagename) {
    currentPage = pagename;
    $('#right_iframe').attr('src', '/pages/' + pagename + '.php?sed=' + randSeed()).show();
}

function iframeLoad(url) {
    $('#right_iframe').attr('src', url + '?sed=' + randSeed());
}

/**
 * 登录验证
 * @returns {undefined}
 */
function loginCheck() {
    if ($('#pd-form-username').val() !== '' && $('#pd-form-password').val() !== '') {
        $.post('/pages/login_validate.php', {
            username: $('#pd-form-username').val(),
            password: $('#pd-form-password').val()
        }, function (res) {
            res = res.toJson();
            $.cookie('username', res.username, {path: '/pages'});
            $.cookie('userid', res.userid, {path: '/pages'});
            $.cookie('usertype', res.usertype, {path: '/pages'});
            if (parseInt(res.status) === 1) {
                location.href = '/pages/facManager.php';
            } else {
                alert('登录失败，用户名或者密码错误！');
            }
        });
    }
}

//回调函数
function timercallback(time) {
    $('#time-span').html(time.join(':'));
}

function Alert(message, warn) {
    warn = warn || false;
    if (warn) {
        $('#__alert__').addClass('warn');
    } else {
        $('#__alert__').removeClass('warn');
    }
    $('#__alert__').slideDown().html(message);
    window.setTimeout(function () {
        $('#__alert__').slideUp();
    }, 1500);
}

/**
 * 退出登陆
 * @returns {undefined}
 */
function logOut() {
    $.cookie("userid", null, {path: '/pages'});
    $.cookie("usertype", null, {path: '/pages'});
    $.cookie("username", null, {path: '/pages'});
    window.location.href = '/';
}

function randSeed() {
    return Math.round(new Date().getTime()/1000);
}