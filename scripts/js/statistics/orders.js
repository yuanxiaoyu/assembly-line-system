/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    $('#customer_orders').height($(window).height() * 0.56 - 55);
    $.get('/pages/statistics/ajax/ajax_getsaledata.php', function(r) {
        r = r.toJson();
        r.b = arrayInt(r.b);
        $('#customer_orders').highcharts({
            title: {
                text: '本月营业额', x: 0, y: 18
            },
            chart: {
                type: 'line',
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                }
            },
            xAxis: {
                categories: r.a,
                lineWidth: 0
            },
            yAxis: {
                title: {
                    text: ''
                },
                minPadding: 0
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: '营业额',
                    data: r.b
                }]
        });
    });

    $.get('/pages/statistics/ajax/ajax_getsalecustomer.php', function(r) {
        r = r.toJson();
        $('#product_orders').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                },
                height: $(window).height() * 0.44
            },
            title: {
                text: '客户订单比例'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                    type: 'pie',
                    name: '占比',
                    data: r.a
                }]
        });
    });

    $.get('/pages/statistics/ajax/ajax_getsaleproductc.php', function(r) {
        r = r.toJson();
        $('#product_orders1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                },
                height: $(window).height() * 0.44
            },
            title: {
                text: '产品销售比例'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                    type: 'pie',
                    name: '占比',
                    data: r.a
                }]
        });
    });
});			