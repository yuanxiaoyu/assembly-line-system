/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {
    $('#customer_orders').height($(window).height() * 0.56 - 55);
    $.get('/pages/statistics/ajax/ajax_getproducedata.php', function(r) {
        r = r.toJson();
        r.b = arrayInt(r.b);
        $('#customer_orders').highcharts({
            title: {
                text: '本月生产量', x: 0, y: 18
            },
            chart: {
                type: 'line',
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                }
            },
            xAxis: {
                categories: r.a,
                lineWidth: 0
            },
            yAxis: {
                title: {
                    text: ''
                },
                minPadding: 0
            },
            legend: {
                borderWidth: 0
            },
            exporting: {
                enabled: false
            },
            series: [{
                    name: '产量',
                    data: r.b
                }, {
                    name: '耗时',
                    data: r.c
                }]
        });
    });

    $.get('/pages/statistics/ajax/ajax_getproducedata_prod.php', function(r) {
        r = r.toJson();
        $('#product_orders1').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                style: {
                    fontFamily: '"Microsoft YaHei"', // default font
                    fontSize: '12px'
                },
                height: $(window).height() * 0.44
            },
            title: {
                text: '产品生产比例'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                    type: 'pie',
                    name: '生产比例',
                    data: r
                }]
        });
    });
});			