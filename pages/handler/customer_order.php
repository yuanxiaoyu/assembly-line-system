<?php

/* 已测试成功
 * 客户订单表的所有操作.
 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/customer_order.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/customer_order_item.php';
$consumer_order_id = $_POST['order_id'];
$customer_id = $_POST['customer_id'];
$order_state = $_POST['state']; //订单状态
$amount = $_POST['amount']; //总金额
$remark = $_POST['remark']; //备注
$products = $_POST['products']; //二维数组：订单详细资料
$statement = $_POST['statement'];   //执行操作判断条件  
$is_delay = $_POST['isdelay'] != '' ? $_POST['isdelay'] : false; // 判断是否延迟发货


switch ($statement) {
    case 'add':
        $orderId = Consumer_order::add_a_order($customer_id, $remark, $products, $amount, $is_delay);
        if ($orderId) {
            echo intval($orderId);
        } else {
            echo 0;
        }
        break;
    case 'delete':
        echo Consumer_order::delete($consumer_order_id);//外键约束，级联删除包括customer_order_item
        break;
    case'update';
        $rs = Consumer_order::get_Consumer_order($consumer_order_id);
        $rs->customer_id = $consumer_order_id;
        $rs->amount = $amount;
        $rs->remark = $remark;
        $rs->order_state = $order_state;
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'save':
        if (Consumer_order::save_consumer_order_item($consumer_order_id, $products)) {
            echo 1;
        } else {
            echo 0;
        }break;

    case 'state_update' :
        $rs = Consumer_order::get_Consumer_order($consumer_order_id);
        $rs->customer_id = $consumer_order_id;
        $rs->order_state = $order_state;
        if ($rs->update_state()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        break;
}