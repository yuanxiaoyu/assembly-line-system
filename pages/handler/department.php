<?php

/* 已测试成功
 * To change this license header, choose License Headers in Project Properties.
 * 插入，更改，删除部门的操作
 * $statement：判断条件
 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/department.php';
$rs = new department();
$statement = $_POST['statement'];
$DepId = $_POST['id'];
$DepName = $_POST['name'];
$password = $_POST['pass'];
$sort = Util::post('sort');

switch ($statement) {
    case 'add':
        if ($rs->department_insert($DepName, $sort, $password)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        if ($rs->department_delete($DepId)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'update':
        if ($rs->department_update($DepId, $DepName, $sort,  $password)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}