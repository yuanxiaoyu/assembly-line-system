<?php

/*
 * 材料需求采购单
 * 新建，删除，修改，材料需求采购单
 * $id材料条目
 * $material_id材料id
 * $number//采购数量

 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Material_demand_item.php';
$statement = $_POST['statement'];
$id == $_POST['id']; //材料条目
$material_demand_id = $_POST['demand_id']; //采购单号
$material_id = $_POST['material_id']; //材料id
$number = $_POST['number']; //采购数量



switch ($statement) {
    case 'add':
        if (Material_demand_item::add_a_material_demand_item($material_id, $material_demand_id, $number)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        if (Material_demand_item::delete_a_material_demand_item($id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'update':
        $rs = Material_demand_item::get_material_demand_item($id);
        $rs->material_demand_id = $material_demand_id;
        $rs->material_id = $material_id;
        $rs->number = $number;
        if ($rs->update()) {
            echo 1;
        } else {

            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}