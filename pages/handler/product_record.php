<?php

/*
 * 增加，删除，产品交易记录
 * $statement:操作判断条件
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_record.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/customer_order.php';
$statement = $_POST['statement'];
$products = $_POST['products']; //二维数组，传入成品的id，价格，数量
$consumer_order_id = intval($_POST['order_id']);

if ($statement == '已发货' || $statement == '已完成') { //增加
    if (Product_record::add_a_record($products)) {//生成交易记录
        $orderInfo = Db::get_instance()->query("SELECT * FROM fac_consumer_order WHERE consumer_order_id = $consumer_order_id;");
        if ($orderInfo[0]['is_proxy'] == 0) {
            Consumer_order::char_state($products); //减去成品库存
        }
        if ($statement == '已完成') {
            $rs = Consumer_order::get_Consumer_order($consumer_order_id);
            $rs->order_state = $statement;
            $rs->update_state();
        }
        echo 1;
    } else {
        echo 0;
    }
} elseif ($statement == 'delete') {//删除
    if (Product_record::del_a_record($trade_id)) {
        echo 1;
    } else {
        echo 0;
    }
}
