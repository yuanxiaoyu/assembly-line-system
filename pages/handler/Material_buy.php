<?php

/*
 * 材料需求采购单
 * 新建，删除，修改，材料需求采购单
 * $id材料条目
 * $material_id材料id
 * $number//采购数量

 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Material_demand.php';
include_once $_SERVER['DOCUMENT_ROOT'] .'/class/department.php';

$statement = $_POST['statement'];
$id = $_POST['id'];
$material_demand_id = $_POST['material_demand_id']; //材料详细条目id

$finish_date = $_POST['finish_date']; //完成时间
$remark = $_POST['remark']; //备注
$operator = $_POST['apply']; //操作人
$materials = $_POST['materials']; //材料二维数组
$price = $_POST['prices']; //总价格
$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

switch ($statement) {
    case 'add':
        if (Material_demand::buy_material_update($materials)&&Material_demand::save_buy_material_record($operator,$materials)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
  
    default :
        echo 0;
        break;
}