<?php

/*
 * 材料需求采购单
 * 新建，删除，修改，材料需求采购单
 * $id材料条目
 * $material_id材料id
 * $number//采购数量

 */

header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Material_demand.php';
include_once $_SERVER['DOCUMENT_ROOT'] .'/class/department.php';

$statement = $_POST['statement'];
$id = $_POST['id'];
$material_demand_id = $_POST['material_demand_id']; //材料详细条目id

$order_state = $_POST['order_state']; //状态
$finish_date = $_POST['finish_date']; //完成时间
$remark = $_POST['remark']; //备注
$apply = $_POST['apply']; //申请人
$demand_date = $_POST['demand_date'];
$materials = $_POST['materials']; //材料二维数组
$price = $_POST['prices']; //总价格
$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

switch ($statement) {
    case 'add':
        if (Material_demand::add_a_order($department_id, $apply, $remark, $materials, $demand_date)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        echo Material_demand::delete_a_material_demand($id);
        break;
    case 'update':
        $rs = Material_demand::get_material_demand_object($id);
        $rs->material_demand_id = $material_demand_id;
        $rs->create_date = $create_date;
        $rs->order_state = $order_state;
        $rs->finish_date = $finish_date;
        $rs->remark = $remark;
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'state_update':
        $rs = Material_demand::get_material_demand_object($id);
        $rs->order_state = $order_state;
        $rs->material_demand_id =$id;
        if($rs->state_update()){
            echo 1;
        }else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}