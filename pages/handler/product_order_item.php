<?php

/*   生产 订单详细表
  $product_order_item_id;id
  $material_id;材料id
  $number;生产产品数量
  $product_order_id;所属订单号

 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_order_item.php';
$statement = $_POST['statement'];
$product_order_item_id = $_POST['product_order_item_id'];
$material_id = $_POST['material_id'];
$number = $_POST['number'];
$product_order_id = $_POST['product_order_id'];


switch ($statement) {
    case 'add':
        if (Product_order_item::add_a_product_order_item($material_id, $number, $product_order_id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;

    case 'delete':
        if (Product_order_item::get_a_page_item($product_order_item_id)) {
            echo 1;
        } else {
            echo 0;
        }
        break;

    case 'update':

        $rs = Product_order_item::get_product_order_item($product_order_item_id);
        $rs->product_order_item_id = $_POST['product_order_item_id'];
        $rs->material_id = $_POST['material_id'];
        $rs->number = $_POST['number'];
        $rs->product_order_id = $_POST['product_order_id'];
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}