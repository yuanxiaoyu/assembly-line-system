<?php

/**
 * 车间取料录入
 */
header('Content-Type: text/html; charset=utf-8');
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/department.php';

$id = Util::post('id');
$statement = Util::post('statement');
$department_id = Util::post('department_id');
$product_id = Util::post('product_id');
$number = Util::post('number');
$wnumber = Util::post('wnumber');
$from_department_id = Util::post('from_department_id');
$order_id = isset($_POST['order_id']) && is_numeric($_POST['order_id']) ? intval($_POST['order_id']) : 0;
//$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

switch ($statement) {
    case 'add':
        $ret = department::saveDepRec($from_department_id, $department_id, $product_id, $number, $wnumber, $order_id);
        if ($ret == -1) {
            echo -1;
        } else if ($ret > 0) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete':
        if (department::delDepRec($id) > 0) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    default :
        echo 0;
        break;
}