<?php

/*
 * 生产订单
 * 新建，删除，修改，生产订单
 *  $id;订单号
  $flow_id;流水线号
  $product_id;产品号
  $number;产品数量
  $create_date;下单时间
  $order_state;订单状态
  $finish_date;完成时间
 */

header('Content-Type: text/html; charset=utf-8');

include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_order.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/class/Product_order_item.php';

$id = Util::post('id');
$statement = Util::post('statement');
$flow_id = 0;
$number = Util::post('number');
$product_id = Util::post('product_id');
$order_state = Util::post('order_state');
$remark = Util::post('remark');
$create_date = Util::post('create_date');
$finish_date = Util::post('finish_date');
$finish_number = Util::post('finish_number');
$code = Util::post('code');
// array,
$materials = Util::post('materials'); //接收的是一个数组
// 2014-10-14
$product_name = Util::post('product_name');
$product_gongyi = Util::post('product_gongyi');

$Db = Db::get_instance();

switch ($statement) {
    case 'add':   // 创建生产订单 
        // 获取product_id通过name,gongyi
        $dt = $Db->query("SELECT `product_id` FROM fac_product_mess WHERE product_model = '$product_name' AND `gongyi` = '$product_gongyi';");
        if (!is_array($dt[0])) {
            $product_id = Product::add_a_product('', trim($product_name), '', $product_gongyi, 0);
        } else {
            $product_id = intval($dt[0]['product_id']);
        }
        if (Product_order::add_a_order($product_id, $code, $number, $flow_id, $remark)) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'delete'://删除
        echo Product_order::delete($id);
        break;
    case 'update'://更新订单
        $rs = Product_order::get_product_order($id);
        $rs->flow_id = $flow_id;
        $rs->number = $number;
        $rs->product_id = $product_id;
        $rs->order_state = $order_state;
        Product_order::save_material_item($id, $materials);
        if ($rs->update()) {
            echo 1;
        } else {
            echo 0;
        }
        break;
    case 'finish':
        $rs = Product_order::get_product_order($id);
        echo $rs->finish($finish_number) ? 1 : 0;
        break;
    default :
        echo 0;
        break;
}