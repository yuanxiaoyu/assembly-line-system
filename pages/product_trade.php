<?php

include dirname(__FILE__) . '/../class/_core.php';
include dirname(__FILE__) . '/../class/Product_record.php';
$row = Product_record::get_a_page_record(0, 0);

foreach ($row as &$od) {
    $od['trade_date'] = Util::timeConv($od['trade_date']);
    $od['price'] = number_format($od['price'], 2);
}

$Smarty->assign('product', $row);
$Smarty->display('product_trade.tpl');
