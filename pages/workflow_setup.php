<?php

include_once dirname(__FILE__) . '/../class/_core.php';
include_once dirname(__FILE__) . '/../class/flow.php';
include dirname(__FILE__) . '/../class/department.php';

$flow = new flow();
$row = $flow->view();
$Smarty->assign('flow', $row);
$rs = new department();
$department = $rs->department_view_all();
$Smarty->assign(department, $department);
$Smarty->display('workflow_setup.tpl');