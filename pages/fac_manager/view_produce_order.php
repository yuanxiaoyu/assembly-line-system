<?php

// 查看生产订单详情

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Product_order.php';

$id = intval($_GET['id']);
$orderData = Product_order::getOrderData($id);
$departmentStatus = Product_order::getOrderFlowsData($id);
$material_list = Product_order::getOrderMeterialList($id);

foreach ($departmentStatus as &$status) {
    if ($status['product_wait'] == 0 && $status['product_sum'] == 0) {
        $status['percent'] = 0.00;
    } else {
        $status['percent'] = sprintf('%.2f',(($status['product_yes'] + $status['product_no']) / $status['product_sum']) * 100);
    }
}

$Smarty->assign('materials', $material_list);
$Smarty->assign('status', $departmentStatus);
$Smarty->assign('orderData', $orderData[0]);
$Smarty->display('fac_manager/view_produce_order.tpl');
