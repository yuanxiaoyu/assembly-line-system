<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';

$dept = Util::get('dept');

// " AND `department_id` <> $dept"

$department = new department();
$view = $department->department_view_all();

$from = Util::get('from');
$to = Util::get('to');
$code = Util::get('pdcode');
$twhere = "";
$month = date('Y-m');

if ($from != "" && $to != "") {
    $twhere = "AND fdr.create_time>='$from' AND fdr.create_time<='$to'";
} else {
    $twhere = " ";
}
if ($code != "") {
    $twhere .= " AND po.product_order_id = '$code' ";
}
foreach ($view as &$v) {
    $v['produce'] = array();
    $departmentId = intval($v['department_id']);
    $orderIds = Db::get_instance()->query("select order_id FROM fac_department_receive_record where department_id = $departmentId UNION select order_id FROM fac_department_product_record where department_id = $departmentId;");
    foreach ($orderIds as $orderId) {
        $orderId = intval($orderId['order_id']);
        $ret = Db::get_instance()->query("SELECT
	(
		SELECT
			SUM(fd1.number)
		FROM
			fac_department_product_record fd1
		WHERE
			fd1.department_id = $departmentId
		AND fd1.order_id = $orderId
	) AS numberX,
	(
		SELECT
			SUM(fd2.number)
		FROM
			fac_department_receive_record fd2
		WHERE
			fd2.department_id = $departmentId
		AND fd2.order_id = $orderId
	) AS getX,
	product_order_code,
	product_model,
	product_code,
	gongyi
FROM
	fac_product_order
LEFT JOIN fac_product_mess fm ON fac_product_order.product_id = fm.product_id
WHERE
	fac_product_order.product_order_id = $orderId;");
        foreach ($ret AS &$p) {
            if ($p['getX'] == 0 || $p['numberX'] == 0) {
                $p['rate'] = false;
            } else {
                $p['rate'] = round(($p['numberX'] / $p['getX']) * 100, 2);
            }
        }
        $v['produce'] = array_merge($v['produce'],$ret);
    }
}

// 获取生产编号
$pCode = Db::get_instance()->query("SELECT product_order_id,product_order_code FROM fac_product_order");

if (department::isFirstDept($dept)) {
    // 获取未完成订单列表
    $pUnfins = Db::get_instance()->query("SELECT
	product_order_code,
	po.number,
	pm.product_model,
	pm.gongyi,
	po.create_date
FROM
	fac_product_order po
LEFT JOIN fac_product_mess pm ON pm.product_id = po.product_id
WHERE
	po.order_state <> '已完成'");

    $Smarty->assign('pUnfins', $pUnfins);
    $Smarty->assign('isfirst', true);
} else {
    $Smarty->assign('isfirst', false);
}

$Smarty->assign('cd', $code);
$Smarty->assign('pCode', $pCode);
$Smarty->assign('from', $from);
$Smarty->assign('to', $to);
$Smarty->assign('dept', $view);
$Smarty->display('workshop/workshop_status.tpl');
