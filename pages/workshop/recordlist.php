<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';

$dept = $_GET['dept'];

$mod = $_GET['mod'];

if ($mod == 'p') {
    $list = department::getPList($dept);
} else {
    $list = department::getGList($dept);
}

foreach ($list as &$od){
    $od['create_time'] = Util::timeConv($od['create_time']);
}

$Smarty->assign('mod', $mod);
$Smarty->assign('list', $list);
$Smarty->display('workshop/recordlist.tpl');
