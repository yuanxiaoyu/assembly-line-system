<?php
include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';
include dirname(__FILE__) . '/../../class/Material_demand.php';

$department_id=department::getUserDepartment(intval($_COOKIE['userid']));
//print_r($department_id);
$row = Material_demand::get_orders_by_departmentid($department_id);

foreach ($row as &$od) {
    $od['create_date'] = Util::timeConv($od['create_date']);
    if($od['finish_date'] == NULL){
        $od['finish_date'] = '无';
    } else {
        $od['finish_date'] = Util::timeConv($od['finish_date']);
    }
}

$Smarty->assign('order', $row);
$Smarty->display('workshop/bus_buy_material.tpl');