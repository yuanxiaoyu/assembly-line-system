<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';
include dirname(__FILE__) . '/../../class/order_department.php';

$department_id = department::getUserDepartment(intval($_COOKIE['userid']));

if (!is_numeric($department_id)) {
    exit();
}

$department = Db::get_instance()->query("SELECT * FROM fac_department WHERE department_id = $department_id;");

// 获取未完成生产订单编号
$pCode = Db::get_instance()->query("SELECT product_order_id,product_order_code,product_id FROM fac_product_order ORDER BY product_order_id DESC;");

$departments = Db::get_instance()->query("SELECT * FROM fac_department WHERE `hidden` = 'no';");
$products = Db::get_instance()->query("SELECT * FROM fac_product_mess WHERE `hidden` = 'no';");

$Smarty->assign('pCode', $pCode);
$Smarty->assign('departments', $departments);
$Smarty->assign('products', $products);
$Smarty->assign('dept', $department[0]);
$Smarty->assign('departmentId', $department_id);
$Smarty->display('workshop/index.tpl');
