<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Product_order.php';
include dirname(__FILE__) . '/../../class/department.php';
include dirname(__FILE__) . '/../../class/order_department.php';

$id = intval($_GET['id']);

$cid = intval($_GET['cid']);

$sort = intval($_GET['sort']);

$time = intval($_GET['time']);

// 部门id
$department_id = $_GET['department_id'];

// 生产订单数据
$orderData = Product_order::getOrderData($id);

// 生产进度百分数
// $percent = Product_order::getOrderMessPercent($department_id, $id);

// 原料
$material_list = Product_order::getOrderMeterialList($id);

// 前一个车间数据
$BeforeState = order_department::view_before_state($sort, $id);  //上车间详情

// 本车间订单数据
$ThisState = order_department::view_one_department_order($cid);

# echo 1;

# var_dump($ThisState);

$Smarty->assign('time', $time);
$Smarty->assign('sort', $sort);
$Smarty->assign('cid', $cid);
// $Smarty->assign('percent', $percent);
$Smarty->assign('department_id', $department_id);
$Smarty->assign('thisstate', $ThisState[0]);
$Smarty->assign('beforestate', $BeforeState[0]);
$Smarty->assign('orderData', $orderData[0]);
$Smarty->display('workshop/view_produce_order.tpl');
