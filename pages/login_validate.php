<?php

require dirname(__FILE__) . '/../class/Users.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 登录ajax验证
 */
$name = $_POST['username'];

$password = $_POST['password'];

$validate = User::login($name, $password);

if ($validate) {
    //    setcookie("usertype", $validate->user_type_id, time() + 360000);
    //    setcookie("userid", $validate->user_id, time() + 360000);
    print_r(json_encode(array(
        'status' => 1,
        'usertype' => $validate->user_type_id,
        'username' => $validate->user_name,
        'userid'   => $validate->user_id
    )));
} else {
    print_r(json_encode(array('status' => 0)));
}