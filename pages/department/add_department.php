<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/department.php';

if ($_GET['mod'] == 'alter') {
    $rs = new department();
    $department = $rs->department_view_one();
    $Smarty->assign('department', $department);
}

$Smarty->assign('mod', $_GET['mod']);
$Smarty->display('department/add_department.tpl');
