<?php

include dirname(__FILE__) . '/../../class/_core.php';

$product = Db::get_instance()->query("SELECT
	department_id,
	product_code,
	product_model,
	gongyi,
	count,
        department_name,
	time
FROM
	fac_product_transfer pt
LEFT JOIN fac_department fd ON fd.department_id = pt.dept_id
LEFT JOIN fac_product_mess pm ON pm.product_id = pt.product_id order by `time` desc");

$Smarty->assign('product', $product);
$Smarty->display('./department/product_transfer_record.tpl');