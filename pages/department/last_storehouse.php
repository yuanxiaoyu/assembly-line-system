<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/LastStoreHouse.php';

$lts = new LastStoreHouse();

$row = $lts->getProductList();
$count = $lts->getProductCount();

$Smarty->assign('count',$count);
$Smarty->assign('product',$row);
$Smarty->display('./department/last_storehouse.tpl');