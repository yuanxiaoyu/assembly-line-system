<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Material.php';
include dirname(__FILE__) . '/../../class/Product.php';
include dirname(__FILE__) . '/../../class/flow.php';

$list = Material::getMaterialsList(0, 0);
$plist = Product::get_a_page_product(0, 0);
$gongyi = array();
$rs= new flow();
$flow=$rs->view();       

foreach($plist as $p){
    if(!in_array($p['gongyi'], $gongyi) && $p['gongyi'] != ''){
        $gongyi[] = $p['gongyi'];
    }
}

$Smarty->assign('flow', $flow);
$Smarty->assign('mlist', $list);
$Smarty->assign('plist', $plist);
$Smarty->assign('gongyi', $gongyi);

$Smarty->display('produce/add_produce_order.tpl');