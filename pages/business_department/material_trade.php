<?php

include $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Material_record.php';

$row=  Material_record::get_a_page_record(20, 0);

foreach ($row as &$od) {
    $od['price'] = number_format($od['price'], 2);
}

$Smarty->assign('material',$row);
$Smarty->display('bus_material_trade.tpl');