<?php
include $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Customer.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Group.php';

$CustomerLev = Group::get_groups();

$Smarty->assign('levels', $CustomerLev);
$Smarty->display('customers_management.tpl');