<?php

include $_SERVER['DOCUMENT_ROOT'] . '/class/_core.php';
include $_SERVER['DOCUMENT_ROOT'] . '/class/Material.php';

$row = Material::getMaterialsList();
$count = Material::getMaterialCount();

$Smarty->assign('mod', $_GET['mod']);
$Smarty->assign('material', $row);
$Smarty->assign('count', $count);
$Smarty->display('bus_material.tpl');