<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dth">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title></title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set" class="clearfix">
            <div style="float: left;overflow-y: auto;overflow-x: hidden;" id="left">
                <div id="flow_product_div" style="width:180px;">
                    <table style="margin-top: 0;" width="100%" id="custab">
                        {foreach from=$levels item=level}
                            <tr class="product_tr">
                                <td data-id="{$level.group_id}">
                                    <a href="javascript:;" class="customerLevBtns" onclick="loadCusDataByGroupId({$level.group_id});">
                                        {$level.group_name}
                                        <i class="del" onclick="deleteGroup({$level.group_id});"></i>
                                        <i class="edit" id="edit_group" rel-data="{$level.group_id}" class="fancybox.ajax" data-fancybox-type="ajax" href="../business_department/modify_customer_level_group.php?mod=modify"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                    </table>
                </div>
                <div style="display: none;">
                    <select id="group_select">  
                        {foreach from=$levels item=dep}
                            <option value ="{$dep.group_id}">{$dep.group_name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div id="flow_flow_div" style="margin-left:179px;border-left: 1px solid #dedede;overflow: auto;margin-bottom: -2px;">
                <table id="custom_det1" style="margin-top: 0;" width="100%">
                    <thead>
                        <tr>
                            <th style="display: none;"></th>
                            <th>姓名</th>
                            <th style="padding-left: 0">电话</th>
                            <th style="padding-left: 0">QQ</th>
                            <th style="padding-left: 0">邮箱</th>
                            <th style="padding-left: 0">地址</th>
                            <th style="display: none" id="group">客户分组</th>
                        </tr>
                    </thead>
                    <tbody>            
                    </tbody>
                </table>
            </div>           
        </div>
        <div id="bottom">
            <div id="product" style="width: 180px;border-right: 1px solid #dedede;margin:-8px -9px;padding:8px 1px;padding-right: 0px;text-align: center;">
                <a class="button" id="group_add"  class="button fancybox.ajax" data-fancybox-type="ajax" href="../business_department/modify_customer_level_group.php?mod=add">添加分组</a>
            </div>
            <div id="setup">
                <a class="button" id="add_customer" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/business_department/add_customer.php?mod=add">添加客户</a>
                <a class="button edit" id="edit_customer" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/business_department/add_customer.php?mod=edit">编辑</a>
                <a class="button del" onclick="customerInfo(3);">删除</a>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                dataTableLis('#custab');
                $('.customerLevBtns').eq(0).click();
                $('#edit_customer').hide();
                dataTableLis('#custom_det1');
                window.onresize = resize;
                resize();
            });
            function resize() {
                $('#iframe_height_set,#flow_flow_div,#left').height(document.documentElement.clientHeight - 42);
            }
        </script>
    </body>
</html>
