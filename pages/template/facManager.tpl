<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link href="/images/www.ico.la_dd375a50fc947087c09b3b2137f70a6b_32X32.ico" rel="Shortcut Icon" />
        <title>{$smarty.cookies.username} - 工厂管理系统</title>
        <link href = "../scripts/css/style.css" rel = "stylesheet" type = "text/css" />
        <script type = "text/javascript" src = "/scripts/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="/scripts/js/main.js"></script>
    </head>
    <body {if $smarty.cookies.usertype eq 4}style="padding-bottom: 0"{/if}>
        {if $smarty.cookies.usertype ne 4}
            {include file="./public/weather.tpl"}
        {/if}
        <div id="__alert__">成品交易记录</div>
        {include file="./public/top_nav.tpl"}
        <div id="wframe">
            <div id="frame">
                {if $smarty.cookies.usertype ne 4}
                    {include file="./public/left_nav.tpl"}
                {/if}
                <div id='right_iframewap'>
                    <iframe id="right_iframe"{if $smarty.cookies.usertype ne 4} style="display: none;border: none;" {/if}src="" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </body>
</html>
