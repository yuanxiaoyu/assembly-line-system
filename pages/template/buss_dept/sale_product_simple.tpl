<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>产品出入库</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <div style="float:left;line-height: 45px;margin-left: 10px;">
                销售出库
            </div>
            <a class="button" href="javascript:;" onclick="goBack();">返回</a>
        </div>
        <div style="width: 400px; margin: 0 auto;" id="iframe_height_set">
            <div class="order_to_produce">             

                <div>
                    <div class="gs-label">产品名称：</div>

                    <select id="tf_product" style="margin-bottom: 8px;">
                        {section name=mi loop=$pds}
                            <option value ="{$pds[mi].product_id}">{$pds[mi].product_model} - {$pds[mi].gongyi}</option>
                        {/section}
                    </select>  

                    <div class="gs-label">客户姓名：</div>
                    <div class="gs-text">
                        <input type="text" id="cusname" value="" />
                    </div>

                    <div class="gs-label">出库数量：</div>
                    <div class="gs-text">
                        <input type="text" id="tf_count" value="" />
                    </div>

                    <div class="gs-label">总金额：</div>
                    <div class="gs-text">
                        <input type="text" id="tf_amount" value="" />
                    </div>

                </div>

                <div class="gs-label">订单备注：</div>
                <span class="frm_textarea_box">
                    <textarea class="js_desc frm_textarea" id="remark"></textarea>
                </span>

            </div>
            <div style="margin-top: 15px;text-align: center;">
                <a class="button edit show" href="javascript:;" data-href="produce_orders" onclick="fastSale();">确认</a>
                <a class='button del show' href='javascript:;' onclick='goBack();'>取消</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
        </script>
    </body>
</html>