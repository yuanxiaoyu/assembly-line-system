<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr class="custom_orders_tr">
                            <th style="display: none">订单id</th>
                            <th>订单号</th>
                            <th>申请车间</th>
                            <th>申请人</th>                          
                            <th>申请时间</th>
                            <th>需求时间</th>
                            <th>处理状态</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$order item=gan}
                            <tr class="custom_orders_tr">
                                <td style="display: none">{$gan.material_demand_id}</td>
                                <td>{$gan.material_demand_code}</td>
                                <td>{$gan.department_name}</td>
                                <td>{$gan.apply}</td>
                                <td>{$gan.create_date}</td> 
                                <td>{$gan.demand_date}</td>   
                                <td>{$gan.order_state}</td>               
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div id="bottom" style="text-align: center;">
            <a href="/pages/business_department/buy_material.php" class="button">采购录入</a>
            <a id="check_detail" class="button edit"  href="/pages/public/check_material_order_detail.php" onclick="check_material_order_detail();">查看详情</a>
            <a id="cusorder_del" class="button del" href="javascript:;" onclick="cancelMaterialOrder()">取消订单</a>
        </div>
    </body>
</html>