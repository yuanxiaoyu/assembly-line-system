<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>原料信息管理</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr>
                            <td>原料名称</td>
                            <td>规格</td>
                            <td>库存</td>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$material item=gan}
                            <tr>
                                <td data-id="{$gan.material_id}">{$gan.material_name}</td>
                                <td>{$gan.material_type}</td>
                                <td>{$gan.number}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>

        </div>
        <div id="bottom">
            <a id="buy" class="button fancybox.ajax" data-fancybox-type="ajax" href="../business_department/bus_buy_material.php">采购</a>
            <a class="button" >退货</a>
            <a class="button" onclick="materialInfoAdd()" href="javascript:;">添加</a>
            <a class="button" onclick="materialInfoAlter()" href="javascript:;">修改</a>
            <a class="button del" onclick="materialInfoDelete();" href="javascript:;">删除</a>
        </div>
    </body>
</html>