<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>工作流程设置</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <input type="hidden" id="nofilter" />
        <div id="iframe_height_set" class="clearfix">
            <div style="float: left;overflow-y: auto;overflow-x: hidden;" id="left">
                <div id="flow_product_div">
                    <table class="tbStyle" style="margin-top: 0;" width="100%" id="wf_name">
                        <thead class="hidden">
                            <tr>
                                <td> </td>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$flow item=step}
                                <tr class="product_tr" style="line-height: 45px;">
                                    <td data-id="{$step.flow_id}"><a href="javascript:;" class="workflow-flows" onclick="loadFlowMessData({$step.flow_id});">{$step.flow_name}</a></td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="flow_flow_div" style="margin-left:234px;border-left: 1px solid #dedede;overflow: auto;margin-bottom: -2px;">
                <table id="workflow-details" class="tbStyle" style="margin-top: 0;" width="100%">
                    <thead>
                        <tr style="line-height: 45px;">
                            <th>流程顺序</th>
                            <th>车间名称</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div id="flow_department_select_div" style="display:none;">
                <select id="flow_department_select" style="width:auto;">  
                    {foreach from=$department item=dep}
                        <option value ="{$dep.department_id}">{$dep.department_name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
        <div id="bottom">
            <div id="product">
                <a class="button" id="ha" class="button fancybox.ajax" data-fancybox-type="ajax" href="../pages/flow_add_product.php?mod=add">添加</a>              
                <a class="button" id="product_modify" class="button fancybox.ajax" data-fancybox-type="ajax" href="../pages/flow_add_product.php?mod=alt">修改</a>
                <a class="button" href="javascript:;" id="flow_flow_delete" onclick="flow_product_delete();">删除</a>
            </div>
            <div id="setup">
                <a class="button" onclick="fnClick(1);">添加</a>
                <a class="button" onclick="fnClick(2);">修改</a>
                <a class="button del" onclick="fnClick(3);">删除</a>
                <a class="button edit" onclick="submit();">保存</a>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                dataTableLis('#wf_name');
                dataTableLis('#workflow-details');
                $('.workflow-flows').eq(0).click();
                window.onresize = resize;
                resize();
            });
            function resize() {
                $('#iframe_height_set,#flow_flow_div,#left').height(document.documentElement.clientHeight - 42);
            }
        </script>
    </body>
</html>
