<div class="fancyBoxIn">
    <div class="gs-label">原料编号：</div>
    <div class="gs-text">
        <input type="text" id="material_code" value="" />
    </div>        

    <div class="gs-label">原料名称：</div>
    <div class="gs-text">
        <input type="text" id="material_name" value="" />
    </div>        

    <div class="gs-label">原料规格：</div>
    <div class="gs-text">
        <input type="text" id="material_type" value="" />
    </div>        

    <div class="gs-label">库存数量：</div>
    <div class="gs-text">
        <input type="text" id="material_count" value="" />
    </div>
    
    <div class="gs-label">原料备注：</div>
    <span class="frm_textarea_box">
        <textarea class="js_desc frm_textarea" id="remark"></textarea>
    </span>     

    <div class="fancyBoxButtons">
        <a class="button" id="materialInfob1">保存</a>
    </div>
</div>