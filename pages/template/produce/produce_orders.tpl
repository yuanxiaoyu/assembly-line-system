<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>生产订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr class="product_orders_tr">
                            <th style="display: none">订单id</th>
                            <th>订单号</th>           
                            <th>产品</th>
                            <th>规格</th>
                                {if !$finishOrderList}
                                <th>预计产量</th>
                                {else}
                                <th>预计产量</th>
                                <th>实际产量</th>
                                <th>良品率</th>
                                {/if}
                            <th>下单时间</th>
                                {if $finishOrderList}
                                <th>完成时间</th>
                                <th>耗时</th>
                                {else}
                                <th>状态</th>
                                {/if}
                        </tr>
                    </thead>
                    <tbody> 
                        {foreach from=$order item=gan}
                            <tr class="product_orders_tr hovTr" onclick="location = '/pages/produce/product_infos.php?id={$gan.product_order_id}';">
                                <td style="display: none">{$gan.product_order_id}</td>
                                <td>{$gan.product_order_code}</td>
                                <td>{$gan.product_model}</td>
                                <td>{$gan.gongyi}</td>
                                {if $finishOrderList}
                                    <td>{$gan.pnumber}</td>
                                    <td>{$gan.finish_number}</td>
                                    <td>{$gan.frate}%</td>
                                {else}
                                    <td>{$gan.pnumber}</td>
                                {/if}
                                <td style="font-size: 12px;">{$gan.create_date}</td>
                                {if $finishOrderList}
                                    <td style="font-size: 12px;">{$gan.finish_date}</td>
                                    <td class="center f12">{$gan.wtime} 天</td>    
                                {else}
                                    <td class="os{$gan.order_stateint}">{$gan.order_state}</td>    
                                {/if}
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div id="fin" style="display:none;line-height: 30px;width:200px;padding:10px;">
            <div id="fin-text"></div>
            <div id="fin-text2"></div>
            <div class="gs-label">实际产量：</div>
            <div class="gs-text">
                <input type="text" id="productNum1" value="" onkeyup="this.value = isNaN(this.value) || isNaN(parseInt(this.value)) ? '' : parseInt(this.value);" />
            </div>
            <div style="text-align: center;margin-top: 15px;">
                <a class="button edit" onclick="produceOrderFinish()" href="javascript:;">生产完成</a>
            </div>
        </div>
    </body>
</html>
