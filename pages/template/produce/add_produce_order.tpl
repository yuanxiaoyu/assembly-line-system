<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <div class="cats">
                下单生产
            </div>
            <a class="button" href="javascript:;" onclick="goBack();">返回</a>
        </div>
        <div style="width: 400px; margin: 0 auto;" id="iframe_height_set">
            <div class="order_to_produce">             

                <div>

                    <div class="gs-label">生产编号：</div>
                    <div class="gs-text">
                        <input type="text" id="code" value="" />
                    </div>

                    <div class="clearfix" style="margin-bottom: 8px;">
                        <div style="float: left;width: 48%">
                            <div class="gs-label">生产产品:</div>
                            <div class="gs-text" style="margin-top: 3px;">
                                <input type="text" id="product_input" value="" />
                            </div>
                            {*<select id="product_input">
                            {section name=mi loop=$plist}
                            <option value ="{$plist[mi].product_id}">{$plist[mi].product_model} - {$plist[mi].gongyi}</option>
                            {/section}
                            </select>*}
                        </div>
                        <div style="float: right;width: 48%">
                            <div class="gs-label">生产规格:</div>
                            <select id="product_gy">
                                <option value="0.2直">0.2直</option>
                                <option value="0.2弧">0.2弧</option>
                                <option value="0.33直">0.33直</option>
                                <option value="0.33弧">0.33弧</option>
                                <option value="0.4直">0.4直</option>
                                <option value="0.4弧">0.4弧</option>
                            </select>  
                        </div>
                    </div>

                    <div class="gs-label">生产数量：</div>
                    <div class="gs-text">
                        <input type="text" id="number" value="" />
                    </div>

                    {*                    <div>选择流程:
                    <select id="flow_select" style="margin-bottom: 8px;">
                    {foreach from=$flow item=gan}
                    <option value ="{$gan.flow_id}">{$gan.flow_name}</option> 
                    {/foreach}
                    </select>
                    </div>*}
                </div>

                <!--  <div style='text-align: center;margin-bottom: 10px;margin-top: 10px;'>
                  <a class="button" id="material_order_select" class="button fancybox.ajax" data-fancybox-type="ajax" href="/pages/public/material_list.php" style='padding:2px 0;width: 200px;'>点击选择原料</a>
                </div>-->

                <div id='productListWrap' style='display: none'>
                    <table id='productListSelected' class='tbStyle' width="100%" style='margin-top: 0;margin-bottom:15px;border:1px solid #dedede;'>
                        <thead>
                            <tr style='border-left: 1px solid #dedede;'>
                                <td>原料编号</td>
                                <td style='max-width: 150px;'>原料</td>
                                <td>规格</td>
                                <td>数量</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>    

                <div class="gs-label">备注</div>
                <span class="frm_textarea_box">
                    <textarea class="js_desc frm_textarea" id="remark"></textarea>
                </span>

            </div>
            <div style="margin-top: 15px;text-align: center;">
                <a class="button edit show" href="javascript:;" data-href="produce_orders" onclick="add_product_orders();">确认下单</a>
                <a class='button del show' href='javascript:;' onclick='goBack();'>取消</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
        </script>
    </body>
</html>