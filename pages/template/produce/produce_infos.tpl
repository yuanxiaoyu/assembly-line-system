<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title> </title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <input type="hidden" id="pdnumber" value="{$pinfo.number}" />
        <input type="hidden" id="pdcode" value="{$pinfo.product_order_code}" />
        <input type="hidden" id="pdid" value="{$pinfo.product_order_id}" />
        <div class="iframeTop">
            <div class="cats">
                生产编号：{$pinfo.product_order_code}&nbsp;&nbsp;
                预计产量：{$pinfo.number}&nbsp;&nbsp;
                生产产品：{$pinfo.product_model}-{$pinfo.gongyi}&nbsp;&nbsp;
                平均良品率：{if $avg_rate}{$avg_rate}{else}0{/if}%&nbsp;&nbsp;
                ({$pinfo.order_state})
            </div>
            <a class="button" href="javascript:;" onclick="goBack();">返回</a>
        </div>
        <div id="iframe_height_set" style="position:relative;">
            {foreach from=$dept item=dep}
                <div style="border-bottom: 1px solid #dedede;padding:12px 15px;">
                    {$dep.department_name} {*<a class="buttonX fancybox.ajax" data-fancybox-type="ajax" href="/pages/workshop/recordlist.php?mod=p&dept={$dep.department_id}">生产详细</a>*}
                    {if $dep.is_first eq 0}
                        {*非分割车间*}
                        <table width="100%" style="border: 1px solid #dedede;margin-top: 12px;">
                            <thead>
                                <tr style="border-left: 1px solid #dedede;">
                                    <th style="width: 25%">型号</th>
                                    <th style="width: 25%">拿料</th>
                                    <th style="width: 25%">实际产出</th>
                                    <th style="width: 25%">良品率</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$dep.produce item=pd name=fo}
                                    <tr style="border-left: 1px solid #dedede;">
                                        <td>
                                            {$pd.product_model}({$pd.gongyi})
                                        </td>
                                        <td>
                                            {$pd.getX}
                                        </td>
                                        <td>
                                            {$pd.numberX}
                                        </td>
                                        <td>
                                            {if $pd.rate}{$pd.rate}%{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    {else}
                        {*分割车间*}
                        <table width="100%" style="border: 1px solid #dedede;margin-top: 12px;">
                            <thead>
                                <tr style="border-left: 1px solid #dedede;">
                                    <th style="width: 25%">型号</th>
                                    <th style="width: 25%">拿料</th>
                                    <th style="width: 25%">开料</th>
                                    <th style="width: 25%">开料比</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$dep.produce item=pd name=fo}
                                    <tr style="border-left: 1px solid #dedede;">
                                        <td>
                                            {$pd.product_model}({$pd.gongyi})
                                        </td>
                                        <td>
                                            {$pd.getX}
                                        </td>
                                        <td>
                                            {$pd.numberX}
                                        </td>
                                        <td>
                                            {if $pd.numberX ne 0.0 and $pd.getX ne 0.0}
                                                1 比 {math equation="y / x" y=$pd.numberX x=$pd.getX format="%.2f"}
                                            {else}
                                                0
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    {/if}
                </div>
            {/foreach}
        </div>
        <div id="fin" style="display:none;line-height: 30px;width:200px;padding:10px;">
            <div id="fin-text"></div>
            <div id="fin-text2"></div>
            <div class="gs-label">实际产量：</div>
            <div class="gs-text">
                <input type="text" id="productNum1" value="" onkeyup="this.value = isNaN(this.value) || isNaN(parseInt(this.value)) ? '' : parseInt(this.value);" />
            </div>
            <div style="text-align: center;margin-top: 15px;">
                <a class="button edit show" onclick="produceOrderFinish()" href="javascript:;">生产完成</a>
            </div>
        </div>
        <div id="bottom" style="text-align: center;">
            {if !$finishOrderList}
                <a class="button edit show" id="producefin" href="#fin">生产完成</a>
                <a class="button del show" onclick="cancelProductOrder()" href="javascript:;">取消订单</a>
            {else}
                <a class="button del show" onclick="cancelProductOrder()" href="javascript:;">删除订单</a>
            {/if}
        </div>
    </body>
</html>