<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>客户订单</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div class="iframeTop">
            <div class="cats">
                修改登录密码
            </div>
            <a class="button" href="javascript:;" onclick="goBack();">返回</a>
        </div>
        <div style="width: 300px; margin: 0 auto;" id="iframe_height_set">
            <div class="order_to_produce">             

                <div>

                    <div class="gs-label">旧密码：</div>
                    <div class="gs-text">
                        <input type="text" id="old_pass" value="" />
                    </div>

                    <div class="gs-label">新密码：</div>
                    <div class="gs-text">
                        <input type="text" id="new_pass" value="" />
                    </div>
                </div>

            </div>
            <div style="margin-top: 35px;text-align: center;">
                <a class="button edit show" href="javascript:;" onclick="change_userpass();">提交</a>
                <a class='button del show' href='javascript:;' onclick='goBack();'>取消</a>
            </div>
        </div>
        <script type='text/javascript'>
            $(function() {
                resize();
                window.onresize = resize;
            });
            function resize() {
                $('#iframe_height_set').css('margin-top', ($(window).height() - $('#iframe_height_set').height()) / 2);
            }
        </script>
    </body>
</html>