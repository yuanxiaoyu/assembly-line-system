<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>原料交易记录</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <table class="dTable" width="100%">
                <thead>  
                    <tr>
                        <th>单号</th>
                        <th>名称</th>
                        <th>规格</th>
                        <th>数量</th>
                        <th>车间</th>
                        <th>时间</th>
                    </tr>
                </thead>
                <tbody> 
                    {foreach from=$material item=trade}
                        <tr>
                            <td>{$trade.id}</td>
                            <td>{$trade.material_name}</td>
                            <td>{$trade.material_type}</td>
                            <td>{$trade.number}</td>
                            <td>{$trade.department_name}</td>
                            <td>{$trade.give_date}</td>                   
                        </tr>
                    {/foreach}
                </tbody>
            </table>
            {if $smarty.cookies.usertype ne 4}
                <div id="bottom" style="text-align: center;">
                    <a href="/pages/business_department/give_material.php" class="button edit show">领料录入</a>
                </div>
            {/if}
        </div>
    </body>
</html>
