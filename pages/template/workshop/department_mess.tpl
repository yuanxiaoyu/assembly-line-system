<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title>分割车间</title>
        {include file="head_iframe.tpl"}
    </head>
    <body>
        <div style="text-align: center;width: 1024px;margin:0 auto;background: #fff;padding-bottom: 20px;"id="frame">

            <div id="department_next" >
                <div id="department_next_top">
                    <a>开始时间：2014-08-05 17:02</a>
                    <a class="button" id="department_next_button" href="javascript:;">返回</a>
                </div>
                <div id="department_next_input_div">
                    <div>
                        <a id="department_next_input_div_top"style="display: block;">材料消耗量、合格品与不合格品录入</a>
                    </div>
                    <div id="department_next_input_div_body">
                        <a id="department_next_input_a">材料消耗量：</a>
                        <input id="department_next_input"></input>
                        <a class="button"  >添加</a>
                        <a id="department_next_input_a">合格品：</a>
                        <input id="department_next_input"></input>
                        <a class="button"  >添加</a>
                        <a id="department_next_input_a">不合格品：</a>
                        <input id="department_next_input"></input>
                        <a class="button"  >添加</a>
                    </div>
                </div>
                <div id="department_next_table1_div">
                    <table >
                        <tbody>          
                            <tr>
                                <td>订单号</td>
                                <td>材料名称</td>
                                <td>材料总数量</td>
                                <td>剩余材料</td>
                                <td>材料消耗</td>

                            </tr>
                            <tr>
                                <td>Apple</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>                                             
                            </tr>
                            <tr>
                                <td>Apple</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>                           
                            </tr>
                            <tr>
                                <td>Apple</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>                            
                            </tr>
                            <tr>
                                <td>Apple</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>
                                <td>Steven Jobs</td>
                                <td>USA</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="department_next_table2_div">
                    <table >
                        <tbody>          
                            <tr class="custom_orders_tr">
                                <td>成品名称</td>
                                <td>型号</td>
                                <td>合格品数量</td>
                                <td>不合格品数量</td>
                                <td>下一车间</td>
                                <td>操作</td>
                            </tr>
                            <tr class="custom_orders_tr">
                                <td>Apple</td>
                                <td>Steven Jobs </td>
                                <td>0</td>
                                <td>Steven Jobs</td>
                                <td>出定型车间</td>
                                <td class="button_td">
                                    <a class="button" >任务完成</a>
                                </td>                     
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="department_next_bottom">
                    <span id="time-span" style="font-size: 20px;"></span>
                    <a class="button" id="timer-start" href="javascript:;">开始</a>
                    <a class="button" id="timer-stop" href="javascript:;">停止</a>
                </div>
            </div>
        </div>
    </body>
</html>