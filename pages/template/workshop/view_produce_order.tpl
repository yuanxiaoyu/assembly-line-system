<input type="hidden" id="department_id" value="{$department_id}" />
<input type="hidden" id="product_order_id" value="{$orderData.product_order_id}" />
<input type="hidden" id="cid" value="{$cid}" />
<input type="hidden" id="sort-val" value="{$sort}" />
<div style='margin:15px;'>
    <div id="department_next_input_div">
        <div>
            <table class="tbStyle" style="width: 100%">
                <tr style="line-height: 30px;">
                    <td class="center">合格品<a id="now_product_yes">{$thisstate.product_yes}</a></td>
                    <td class="center">不合格<a id="now_product_no"> {$thisstate.product_no}</a></td>
                    <td class="center">待生产<a id="product_wait">{$thisstate.product_wait}</a></td>
                </tr>
            </table>
        </div>
        <div class="clearfix" style="padding:18px;padding-bottom: 0">
            <div style="float:left;width:48%;">
                <div class="gs-label">合格数量：</div>
                <div class="gs-text">
                    <input type="text" id="product_yes" value="" onkeyup="checkLimit(this, this.value,{$thisstate.product_wait});" />
                </div>     
            </div>
            <div style="float:right;width:48%;">
                <div class="gs-label">不合格数：</div>
                <div class="gs-text">
                    <input type="text" id="product_no" value="0" onkeyup="checkLimit(this, this.value,{$thisstate.product_wait});" />
                </div>     
            </div>
        </div>
        <div id="department_next_input_div_body">
            <div style="text-align: center;margin:15px auto;">
                <a class="button edit" style="display:inline-block;" href="javascript:;" onclick="product_yes_add();">确认提交</a>
            </div>
        </div>
        <input style="display:none;" value="{$time}" id="time_input" />
    </div>
    {*    <div>
    <table width="100%" style="border-top: 1px solid #dedede;border-right: 1px solid #dedede;box-shadow: 0 1px 3px rgba(0,0,0,0.1);;">
    <thead>
    <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
    <td>订单号</td>
    <td>成品名称</td>                           
    <td>材料</td>
    <td>规格</td>
    <td>数量</td>
    <td>流水线</td>
    </tr>
    </thead>
    <tbody> 
    <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
    <td id="product_order_id">{$orderData.product_order_id}</td>
    <td>{$orderData.product_model}</td>
    <td>{$orderData.product_material}</td>
    <td>{$orderData.gongyi}</td>
    <td id="product_order_number">{$orderData.pnumber}</td>
    <td>{$orderData.flow_name}</td> 
    </tr>
    </tbody>
    </table>
    </div>*}
    <div id="last_department_details_div">
        <table width="100%" style="border-top: 1px solid #dedede;border-right: 1px solid #dedede;box-shadow: 0 1px 3px rgba(0,0,0,0.1);;">
            <thead>
                <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                    <td>上一车间</td>
                    <td>合格品</td>                        
                    <td>不合格</td>                      
                    <td>待生产</td>                     
                    <td>状态</td>    
                </tr>
            </thead>
            <tbody> 
                <tr class="product_orders_tr" style="border-left: 1px solid #dedede;">
                    <td>{$beforestate.department_name}</td>
                    <td id="front_product_yes">{$beforestate.product_yes}</td>
                    <td id="front_product_no">{$beforestate.product_no}</td>
                    <td id="front_product_wait">{$beforestate.product_wait}</td>
                    <td id='lastStatus'>{$beforestate.taskstate}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <span id="time-span" style="font-size: 20px;display: block;text-align: center;padding: 5px 0;"></span>
    <div id="department_next_bottom" style='text-align: center;'>
        {*        <a class="button" id="timer-start" href="javascript:;" onclick="state_change(1);">开始</a>
        <a class="button" id="timer-stop" href="javascript:;" onclick="state_change(2);">停止</a>*}
        <a class="button edit hidden" id="task-finish" href="javascript:;" onclick="state_change(3);">任务完成</a>
    </div>
</div>