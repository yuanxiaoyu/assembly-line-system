<div id="iframe_height_set" style="position:relative;">
    {if $isfirst}
        <div style="border-bottom: 1px solid #dedede;padding:12px 15px;">
            <b>未完成生产订单</b>
            <table width="100%" class="dTable pdtable" style="border: 1px solid #dedede;margin-top: 12px;">
                <thead>
                    <tr style="border-left: 1px solid #dedede;">
                        <th style="width: 20%">生产编号</th>
                        <th style="width: 20%">产品</th>
                        <th style="width: 20%">数量</th>
                        <th style="width: 20%">下单时间</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach from=$pUnfins item=pd name=fo}
                        <tr style="border-left: 1px solid #dedede;">
                            <td class="f12">{$pd.product_order_code}</td>
                            <td class="f12">{$pd.product_model}({$pd.gongyi})</td>
                            <td class="f12">{$pd.number}</td>
                            <td class="f12">{$pd.create_date|date_format:'%Y-%m-%d'}</td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
    {/if}
    {foreach from=$dept item=dep}
        <div style="border-bottom: 1px solid #dedede;padding:12px 15px;">
            {$dep.department_name} {*<a class="buttonX fancybox.ajax" data-fancybox-type="ajax" href="/pages/workshop/recordlist.php?mod=p&dept={$dep.department_id}">生产详细</a>*}
            {if $dep.is_first eq 0}
                {*非分割车间*}
                <table width="100%" class="dTable pdtable" style="border: 1px solid #dedede;margin-top: 12px;">
                    <thead>
                        <tr style="border-left: 1px solid #dedede;">
                            <th style="width: 20%">生产编号</th>
                            <th style="width: 20%">型号</th>
                            <th style="width: 20%">拿料</th>
                            <th style="width: 20%">实际产出</th>
                            <th style="width: 20%">良品率</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$dep.produce item=pd name=fo}
                            <tr style="border-left: 1px solid #dedede;">
                                <td class="f12">
                                    {$pd.product_order_code}
                                </td>
                                <td class="f12">
                                    {$pd.product_model}({$pd.gongyi})
                                </td>
                                <td class="f12">
                                    {$pd.getX}
                                </td>
                                <td class="f12">
                                    {$pd.numberX}
                                </td>
                                <td class="f12">
                                    {if $pd.rate}{$pd.rate}%{/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            {else}
                {*分割车间*}
                <table width="100%" class="dTable pdtable" style="border: 1px solid #dedede;margin-top: 12px;">
                    <thead>
                        <tr style="border-left: 1px solid #dedede;">
                            <th style="width: 20%">生产编号</th>
                            <th style="width: 20%">型号</th>
                            <th style="width: 20%">拿料</th>
                            <th style="width: 20%">开料</th>
                            <th style="width: 20%">开料比</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$dep.produce item=pd name=fo}
                            <tr style="border-left: 1px solid #dedede;">
                                <td class="f12">
                                    {$pd.product_order_code}
                                </td>
                                <td class="f12">
                                    {$pd.product_model}({$pd.gongyi})
                                </td>
                                <td class="f12">
                                    {$pd.getX}
                                </td>
                                <td class="f12">
                                    {$pd.numberX}
                                </td>
                                <td class="f12">
                                    {if $pd.numberX ne 0.0 and $pd.getX ne 0.0}
                                        1 比 {math equation="y / x" y=$pd.numberX x=$pd.getX format="%.2f"}
                                    {else}
                                        0
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            {/if}
        </div>
    {/foreach}
</div>