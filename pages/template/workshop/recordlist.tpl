<table class="dTable pdtable" width="100%" style='margin-top: 0;max-height: 400px;'>
    <thead>
        <tr style="font-size: 12px;">
            <th>生产编号</th>
            <th>名称</th>
            <th>数量</th>
                {if $mod eq 'g'}
                <th>车间</th>
                {/if}
            <th>工号</th>
            <th>时间</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
        {strip}
            {foreach from=$list item=l}
                <tr>
                    <td class='f12'>{if $l.product_order_code}{$l.product_order_code}{else}无{/if}</td>
                    <td class='f12' style="color:#444">{$l.product_model}({$l.gongyi})</td>
                    <td class='f12'>{$l.number}</td>
                    {if $mod eq 'g'}
                        <td class='f12'>{$l.department_name}</td>
                    {/if}
                    <td class='f12'>{$l.wnumber}</td>
                    <td class='f12'>{$l.create_time}</td>
                    <td class='f12'><a href='javascript:;' onclick='recordDelete({$l.id}, "{$mod}", this);' style="color:#900">删</a></td>
                </tr>
            {/foreach}
        {/strip}
    </tbody>
</table>    