<div id="topnav">
    <div id="navIn">
        <div class="welcome" style="float: left;">
            欢迎！{$userName}&nbsp;今天是 {$today}
        </div>
        <div style="float:right" class="clearfix">
            <a class="topNavItem" href="javascript:;" onclick="logOut();">退出</a>
            <a class="topNavItem" href="javascript:;" data-href="/user/changepass">修改密码</a>
            {if $usertype == 5}
                <a class="topNavItem" href="javascript:;" data-href="business_department/create_customer_order">客户下单</a>
            {/if}
            {if $usertype == 1 || $usertype == 5}
                <a class="topNavItem" href="javascript:;" data-href="produce/add_produce_order">下单生产</a>
                <a class="topNavItem" href="javascript:;" data-href="product_storehouse">查看库存</a>
            {else if $usertype <= 2}
                <a class="topNavItem" href="javascript:;" data-href="business_department/create_customer_order">客户下单</a>
                <a class="topNavItem" href="javascript:;" data-href="business_department/product_storehouse">查看库存</a>
            {else if $usertype == 4}
                <a class="topNavItem" href="javascript:;" data-href="workshop/index">生产录入</a>
                <a class="topNavItem" href="javascript:;" data-href="workshop/bus_buy_material">采购申请</a>
                <a class="topNavItem" href="javascript:;" data-href="business_department/department_material_record"><b>拿料记录</b></a>
            {/if}
        </div>
    </div>
</div>