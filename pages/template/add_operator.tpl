<div style="padding:5px 20px;width:300px;">
    <div class="add_operator">

        <div class="gs-label">姓名</div>
        <div class="gs-text">
            <input type="text" id="operator_name" />
        </div>

        <div class="gs-label">密码</div>
        <div class="gs-text">
            <input type="text" id="operator_password" />
        </div>

        <div class="gs-label">车间</div>

        <div class="Center" style="margin-bottom: 8px;">
            <select id="operator_type_id" onclick="show_all();">  
                {section name=sc loop=$opts}
                    {if $smarty.cookies.usertype < $opts[sc].user_type_id}
                        <option value ="{$opts[sc].user_type_id}" >{$opts[sc].user_type_name}</option>
                    {/if}
                {/section}
            </select>  
        </div>
            
        <div id="department_div" style="display:none;">
            <div class="gs-label">选择车间</div>
            <select id="department_select">  
                {foreach from=$departments item=dep}
                    <option value ="{$dep.department_id}" >{$dep.department_name}</option>
                {/foreach}
            </select> 
        </div>
            
    </div>
    <div class="Center" style="margin-top: 20px;">
        {if $mod == 'add'}
            <a class="button" id="confirm_add" href="javascript:;"  onclick="operatorAdd();">确认添加</a>
        {else}
            <a class="button"  id="save_modify" href="javascript:;"  onclick="operatorSave();">保存修改</a>
        {/if}
    </div>
</div>