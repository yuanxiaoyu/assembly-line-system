<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>{$title}</title>
        {include file="head_iframe.tpl"}
        <script type="text/javascript" src="/scripts/js/highcharts/js/highcharts.js"></script>
        <script type="text/javascript" src="/scripts/js/statistics/orders.js"></script>
    </head>
    <body class="iframe_body">
        <div id="customer_orders" class="h70"></div>
        <div class="clearfix h30" style="border-top: 1px solid #dedede;">
            <div style="width: 50%;float:left;margin: 0 0%;" id="product_orders"></div>
            <div style="width: 50%;float:right;margin: 0 0%;" id="product_orders1"></div>
        </div>
        <div id="bottom" style="text-align: center;">
            <a class="button" href="/pages/business_department/customers_orders.php">查看本月客户订单</a>
        </div>
    </body>
</html>