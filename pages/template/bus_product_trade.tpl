<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>成品交易记录</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body" style="margin-bottom: 0;">
        <div id="iframe_height_set">
            <table class="dTable" width="100%">
                <thead>
                    <tr>
                        <td>单号</td>
                        <td>名称</td>
                        <td>材料</td>
                        <td>数量</td>
                        <td>金额</td>
                        <td>时间</td>
                    </tr>
                </thead>
                <tbody>     
                    {foreach from=$product item=trade}
                        <tr>
                            <td>{$trade.trade_id}</td>
                            <td>{$trade.product_model}</td>
                            <td>{$trade.product_material}</td>
                            <td>{$trade.trade_number}</td>
                            <td>{$trade.price}</td>
                            <td>{$trade.trade_date}</td>                   
                        </tr>            
                    {/foreach}
                </tbody>
            </table>
        </div>
    </body>
</html>
