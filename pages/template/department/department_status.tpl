<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title> </title>
        {include file="head_iframe.tpl"}
        <link rel="stylesheet" type="text/css" href="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.css" />
        <script src="/scripts/js/jquery-ui-1.11.1.custom/jquery-ui.min.js"></script>
        <script src="/scripts/js/jquery-ui-1.11.1.custom/cn.js"></script>
    </head>
    <body class="iframe_body" style="padding:0;">
        <div id="iframe_height_set" style="position:relative;">
            <form style="position:absolute;right:15px;top:10px;" action="#" id="statusForm" method="GET">
                <a class='button edit show' href="javascript:;" style="float:right;margin-top: 2px;" onclick="$('#statusForm').submit()">查询</a>
                <div class="gs-text"style="width: 150px;display: inline-block; float:right" >
                    <input type="text" id="order-delay2" value="{$to}" name="to" placeholder='选择查询日期' readonly="true" />
                </div>                     
                <div class="gs-text"style="width: 150px;display: inline-block; float:right;margin-right: 10px;" >
                    <input type="text" id="order-delay1" value="{$from}" name="from" placeholder='选择查询日期' readonly="true" />
                </div>   
                <div style="width: 150px;display: inline-block; float:right;margin-right: 10px;" >
                    <select style="margin:0;" onchange="$('#statusForm').submit()" name="pdcode">
                        <option value ="">不筛选编号</option>
                        {foreach from=$pCode item=code}
                            <option value ="{$code.product_order_id}" {if $cd eq $code.product_order_id}selected="selected"{/if} >编号{$code.product_order_code}</option>
                        {/foreach}
                    </select>
                </div> 
            </form>
            {foreach from=$dept item=dep}
                <div style="border-bottom: 1px solid #dedede;padding:15px;">
                    {$dep.department_name} {*<a class="buttonX fancybox.ajax" data-fancybox-type="ajax" href="/pages/workshop/recordlist.php?mod=p&dept={$dep.department_id}">生产详细</a>*}
                    {if $dep.is_first eq 0}
                        {*非分割车间*}
                        <table width="100%" style="border: 1px solid #dedede;margin-top: 15px;">
                            <thead>
                                <tr style="border-left: 1px solid #dedede;">
                                    <th style="width: 20%">生产编号</th>
                                    <th style="width: 20%">型号</th>
                                    <th style="width: 20%">拿料</th>
                                    <th style="width: 20%">实际产出</th>
                                    <th style="width: 20%">良品率</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$dep.produce item=pd name=fo}
                                    <tr style="border-left: 1px solid #dedede;">
                                        <td>
                                            {$pd.product_order_code}
                                        </td>
                                        <td>
                                            {$pd.product_model}({$pd.gongyi})
                                        </td>
                                        <td>
                                            {$pd.getX}
                                        </td>
                                        <td>
                                            {$pd.numberX}
                                        </td>
                                        <td>
                                            {if $pd.rate}{$pd.rate}%{/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    {else}
                        {*分割车间*}
                        <table width="100%" style="border: 1px solid #dedede;margin-top: 15px;">
                            <thead>
                                <tr style="border-left: 1px solid #dedede;">
                                    <th style="width: 20%">生产编号</th>
                                    <th style="width: 20%">型号</th>
                                    <th style="width: 20%">拿料</th>
                                    <th style="width: 20%">开料</th>
                                    <th style="width: 20%">开料比</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach from=$dep.produce item=pd name=fo}
                                    <tr style="border-left: 1px solid #dedede;">
                                        <td>
                                            {$pd.product_order_code}
                                        </td>
                                        <td>
                                            {$pd.product_model}({$pd.gongyi})
                                        </td>
                                        <td>
                                            {$pd.getX}
                                        </td>
                                        <td>
                                            {$pd.numberX}
                                        </td>
                                        <td>
                                            {if $pd.numberX ne 0.0 and $pd.getX ne 0.0}
                                                1 比 {math equation="y / x" y=$pd.numberX x=$pd.getX format="%.2f"}
                                            {else}
                                                0
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    {/if}
                </div>
            {/foreach}
        </div>
        <script type='text/javascript'>
            $(function() {
                initdatepicker_cn();
                $('#order-delay1').datepicker({
                    //navigationAsDateFormat: true,
                    dateFormat: 'yy-mm-dd'
                });
                $('#order-delay2').datepicker({
                    //navigationAsDateFormat: true,
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
    </body>
</html>