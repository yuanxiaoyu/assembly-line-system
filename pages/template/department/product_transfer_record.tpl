<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <title>成品仓库</title>
        {include file="head_iframe.tpl"}
    </head>
    <body class="iframe_body">
        <div id="iframe_height_set">
            <div>
                <table class="dTable" width="100%">
                    <thead>
                        <tr>
                            <th style="display: none;">商品id</th>
                            <th>商品编号</th>
                            <th>商品名称</th>
                            <th>规格</th>
                            <th>入库数量</th>
                            <th>入库时间</th>
                            <th>入库车间</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$product item=house}
                            <tr>
                                <td style="display: none;">{$house.product_id}</td>
                                <td>{$house.product_code}</td>
                                <td>{$house.product_model}</td>
                                <td>{$house.gongyi}</td>
                                <td>{$house.count}</td>
                                <td>{$house.time}</td>
                                <td>{$house.department_name}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
        <div id="bottom">
            <a id="productTransfer" class="button edit show" href="/pages/department/product_transfer.php?mod=1">生产出库</a>
        </div>
    </body>
</html>

