<div style="width: 200px;padding:20px;">
    <div id="modify_department_div">
        <div class="gs-label">车间名称：</div>
        <div class="gs-text">
            <input type="text" id="department_name" value="" />
        </div>        
        <div class="gs-label">车间顺序：</div>
        <div class="gs-text">
            <input type="text" id="sort" value="" />
        </div>
        <div class="gs-label">账户密码：</div>
        <div class="gs-text">
            <input type="text" id="password" value="" />
        </div>
    </div>
    <div style="margin-top: 15px;text-align: center;">
        {if $mod == 'add'}
            <a class="button" id="confirm_add" href="javascript:;"  onclick="departmentAdd();">确认添加</a> 
        {else}
            <a class="button" id="save_modify" href="javascript:;"  onclick="departmentSave();">保存修改</a>
        {/if}
    </div>
</div>