<?php

include dirname(__FILE__) . '/../../class/_core.php';
include dirname(__FILE__) . '/../../class/Stats/Overview.php';

$QueryMonth = isset($_GET['month']) ? $_GET['month'] : date("Y-m");
$QueryDate = isset($_GET['date']) ? $_GET['date'] : date("Y-m-d");

$Overview = new Overview($QueryMonth);

// 获取销量数据
$data1 = $Overview->getProductOverView();

// 获取部门数据
$data2 = $Overview->getDepartmentDatas();

foreach($data2 as &$d){
    
}

foreach($data1 as &$d){
    $d['msale'] = number_format($d['msale'], 2);
}

$Smarty->assign('data1', $data1);
$Smarty->assign('data2', $data2);
$Smarty->display('statistics/overviews.tpl');