/*
Navicat MySQL Data Transfer

Source Server         : 112.124.44.172
Source Server Version : 50532
Source Host           : 112.124.44.172:3306
Source Database       : factory

Target Server Type    : MYSQL
Target Server Version : 50532
File Encoding         : 65001

Date: 2014-12-24 16:38:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fac_consumer_order
-- ----------------------------
DROP TABLE IF EXISTS `fac_consumer_order`;
CREATE TABLE `fac_consumer_order` (
  `consumer_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_order_code` varchar(30) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `send_date` date DEFAULT NULL,
  `order_state` enum('已完成','已发货','待发货','未处理','待生产') DEFAULT '未处理',
  `finish_date` datetime DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL COMMENT '总金额',
  `is_proxy` tinyint(4) DEFAULT '0',
  `del` int(11) DEFAULT '0' COMMENT '删除标记，1表示该行数据已删除',
  PRIMARY KEY (`consumer_order_id`),
  KEY `customer_id` (`customer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=479 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`customer_id`) REFER `assembly_line_system/fac_customer`(';

-- ----------------------------
-- Records of fac_consumer_order
-- ----------------------------
INSERT INTO `fac_consumer_order` VALUES ('464', 'C20141023154429', '2117', '2014-10-23 15:44:29', '2014-10-24', '待生产', null, '', '240.00', '0', '0');
INSERT INTO `fac_consumer_order` VALUES ('465', 'C20141023154432', '2117', '2014-10-23 15:44:32', '2014-10-24', '待生产', null, '', '240.00', '0', '0');
INSERT INTO `fac_consumer_order` VALUES ('466', 'C20141023154438', '2117', '2014-10-23 15:44:38', '2014-10-24', '待生产', null, '', '24.00', '0', '0');
INSERT INTO `fac_consumer_order` VALUES ('477', 'C20141109122639', '2117', '2014-11-09 12:26:39', '2014-11-06', '已发货', '2014-11-09 12:27:01', '', '600.00', '1', '0');
INSERT INTO `fac_consumer_order` VALUES ('478', 'C20141109122945', '2117', '2014-11-09 12:29:45', '2014-11-05', '已发货', '2014-11-09 12:29:51', '', '600.00', '1', '0');

-- ----------------------------
-- Table structure for fac_consumer_order_item
-- ----------------------------
DROP TABLE IF EXISTS `fac_consumer_order_item`;
CREATE TABLE `fac_consumer_order_item` (
  `consumer_order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `number` int(20) DEFAULT NULL,
  `consumer_order_id` int(11) DEFAULT NULL,
  `unit_price` float(10,2) DEFAULT NULL,
  `unit_price2` float(10,2) DEFAULT '0.00',
  PRIMARY KEY (`consumer_order_item_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  KEY `consumer_order_id` (`consumer_order_id`) USING BTREE,
  CONSTRAINT `fac_consumer_order_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`),
  CONSTRAINT `fac_consumer_order_item_ibfk_2` FOREIGN KEY (`consumer_order_id`) REFERENCES `fac_consumer_order` (`consumer_order_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`product_id`) REFER `assembly_line_system/fac_product_mes';

-- ----------------------------
-- Records of fac_consumer_order_item
-- ----------------------------
INSERT INTO `fac_consumer_order_item` VALUES ('8', '230', '100', '477', '6.00', '6.00');
INSERT INTO `fac_consumer_order_item` VALUES ('9', '230', '100', '478', '6.00', '6.00');

-- ----------------------------
-- Table structure for fac_consumer_order_proxy
-- ----------------------------
DROP TABLE IF EXISTS `fac_consumer_order_proxy`;
CREATE TABLE `fac_consumer_order_proxy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `amount` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_consumer_order_proxy
-- ----------------------------
INSERT INTO `fac_consumer_order_proxy` VALUES ('1', '45456', '460', '10.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('2', '45456', '461', '10.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('3', '45456', '463', '4.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('4', '45456', '464', '4.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('5', '45456', '465', '4.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('6', '45456', '466', '4.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('7', '', '477', '600.00');
INSERT INTO `fac_consumer_order_proxy` VALUES ('8', '汇集华科', '478', '600.00');

-- ----------------------------
-- Table structure for fac_customer
-- ----------------------------
DROP TABLE IF EXISTS `fac_customer`;
CREATE TABLE `fac_customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `qq` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `tel` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `e_mail` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `remark` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `hidden` enum('no','yes') CHARACTER SET utf8 DEFAULT 'no',
  PRIMARY KEY (`customer_id`),
  KEY `group_id` (`group_id`) USING BTREE,
  CONSTRAINT `fac_customer_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `fac_group` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2122 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='InnoDB free: 3072 kB; (`group_id`) REFER `assembly_line_system/fac_group`(`group';

-- ----------------------------
-- Records of fac_customer
-- ----------------------------
INSERT INTO `fac_customer` VALUES ('2116', '刘勇', '1', '356612618', '13802414177', '356612618@qq.com', '广州大学城', '', 'yes');
INSERT INTO `fac_customer` VALUES ('2117', 'SEKO 马小姐', '1', '', '13724372883', '', '', '', 'no');

-- ----------------------------
-- Table structure for fac_department
-- ----------------------------
DROP TABLE IF EXISTS `fac_department`;
CREATE TABLE `fac_department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` char(20) DEFAULT NULL,
  `fpyrate` float(5,2) DEFAULT '95.00',
  `is_first` tinyint(4) DEFAULT '0',
  `is_final` tinyint(4) DEFAULT '0',
  `sort` tinyint(4) DEFAULT '0',
  `hidden` enum('no','yes') DEFAULT 'no' COMMENT '外键删除时，隐藏',
  `add_time` datetime DEFAULT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_department
-- ----------------------------
INSERT INTO `fac_department` VALUES ('42', '分割车间', '95.00', '0', '0', '0', 'yes', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('43', '钢化车间', '95.00', '0', '0', '0', 'yes', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('44', '检验车间', '95.00', '0', '0', '0', 'yes', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('45', '开料车间', '95.00', '1', '0', '1', 'no', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('46', '出型车间', '95.00', '0', '0', '0', 'yes', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('47', '印花车间', '95.00', '0', '0', '0', 'yes', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('53', '贴合车间', '95.00', '0', '1', '10', 'no', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('55', 'CNC车间', '95.00', '0', '0', '2', 'no', '2014-10-08 23:45:26');
INSERT INTO `fac_department` VALUES ('57', '扫光车间', '95.00', '0', '0', '3', 'no', '2014-10-12 12:43:45');
INSERT INTO `fac_department` VALUES ('58', '钢化前质检', '95.00', '0', '0', '5', 'no', '2014-10-12 12:46:21');
INSERT INTO `fac_department` VALUES ('59', '钢化转架', '95.00', '0', '0', '6', 'no', '2014-10-12 12:46:39');
INSERT INTO `fac_department` VALUES ('60', '包装车间', '95.00', '0', '0', '0', 'yes', '2014-10-12 12:48:53');
INSERT INTO `fac_department` VALUES ('61', '钢化后超声波', '95.00', '0', '0', '7', 'no', '2014-10-12 19:01:15');
INSERT INTO `fac_department` VALUES ('62', '扫光后超声波', '95.00', '0', '0', '4', 'no', '2014-11-12 16:48:18');
INSERT INTO `fac_department` VALUES ('63', '钢化后质检', '95.00', '0', '0', '8', 'no', '2014-11-12 16:53:22');
INSERT INTO `fac_department` VALUES ('64', '指纹油', '95.00', '0', '0', '9', 'no', '2014-11-12 16:54:29');
INSERT INTO `fac_department` VALUES ('65', '成品车间', '95.00', '0', '0', '11', 'no', '2014-11-12 16:55:22');

-- ----------------------------
-- Table structure for fac_department_product_record
-- ----------------------------
DROP TABLE IF EXISTS `fac_department_product_record`;
CREATE TABLE `fac_department_product_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `wnumber` varchar(255) COLLATE utf8_bin DEFAULT '',
  `number` float(11,1) DEFAULT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fac_department_receive_record_ibfk_1` (`department_id`) USING BTREE,
  KEY `fac_department_receive_record_ibfk_2` (`product_id`) USING BTREE,
  CONSTRAINT `fac_department_product_record_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`),
  CONSTRAINT `fac_department_product_record_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `fac_department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_department_product_record
-- ----------------------------
INSERT INTO `fac_department_product_record` VALUES ('244', '60', '93', '', '1050.0', '186', '2014-10-12 12:53:55');
INSERT INTO `fac_department_product_record` VALUES ('268', '58', '231', '', '1100.0', '200', '2014-10-15 22:30:07');
INSERT INTO `fac_department_product_record` VALUES ('269', '58', '231', '', '1100.0', '194', '2014-10-15 22:30:40');
INSERT INTO `fac_department_product_record` VALUES ('270', '59', '231', '', '1080.0', '200', '2014-10-15 22:46:07');
INSERT INTO `fac_department_product_record` VALUES ('272', '59', '231', '', '1080.0', '194', '2014-10-15 22:46:33');
INSERT INTO `fac_department_product_record` VALUES ('273', '53', '231', '', '1050.0', '200', '2014-10-15 22:47:18');
INSERT INTO `fac_department_product_record` VALUES ('274', '53', '231', '', '1050.0', '194', '2014-10-15 22:47:43');
INSERT INTO `fac_department_product_record` VALUES ('275', '61', '231', '', '1000.0', '200', '2014-10-15 22:48:30');
INSERT INTO `fac_department_product_record` VALUES ('276', '61', '231', '', '1000.0', '194', '2014-10-15 22:48:36');
INSERT INTO `fac_department_product_record` VALUES ('277', '45', '94', '', '600.0', '201', '2014-10-20 15:20:05');
INSERT INTO `fac_department_product_record` VALUES ('278', '55', '94', '', '580.0', '201', '2014-10-20 15:25:48');
INSERT INTO `fac_department_product_record` VALUES ('279', '57', '94', '', '550.0', '201', '2014-10-20 15:27:58');
INSERT INTO `fac_department_product_record` VALUES ('280', '45', '248', '001', '1200.0', '205', '2014-10-27 23:18:13');
INSERT INTO `fac_department_product_record` VALUES ('281', '55', '248', '002', '450.0', '205', '2014-10-27 23:28:43');
INSERT INTO `fac_department_product_record` VALUES ('282', '55', '248', '002', '650.0', '205', '2014-10-27 23:31:22');
INSERT INTO `fac_department_product_record` VALUES ('283', '57', '248', '003', '1000.0', '205', '2014-10-27 23:32:59');
INSERT INTO `fac_department_product_record` VALUES ('284', '45', '248', '8888', '1200.0', '206', '2014-10-30 00:20:09');
INSERT INTO `fac_department_product_record` VALUES ('285', '55', '248', '9999', '480.0', '206', '2014-10-30 00:21:55');
INSERT INTO `fac_department_product_record` VALUES ('286', '55', '248', '9999', '650.0', '206', '2014-10-30 00:22:31');
INSERT INTO `fac_department_product_record` VALUES ('287', '45', '94', '', '1200.0', '207', '2014-10-31 02:53:49');
INSERT INTO `fac_department_product_record` VALUES ('288', '55', '94', '', '500.0', '207', '2014-10-31 02:54:28');
INSERT INTO `fac_department_product_record` VALUES ('289', '55', '94', '', '700.0', '207', '2014-10-31 02:54:49');
INSERT INTO `fac_department_product_record` VALUES ('290', '55', '248', '', '1150.0', '208', '2014-11-07 20:14:28');
INSERT INTO `fac_department_product_record` VALUES ('291', '45', '248', '', '1200.0', '208', '2014-11-07 20:15:20');
INSERT INTO `fac_department_product_record` VALUES ('292', '57', '248', '', '1150.0', '208', '2014-11-08 22:52:34');
INSERT INTO `fac_department_product_record` VALUES ('293', '45', '251', '', '998.0', '209', '2014-11-08 22:54:55');
INSERT INTO `fac_department_product_record` VALUES ('294', '55', '251', '', '998.0', '209', '2014-11-11 14:34:17');
INSERT INTO `fac_department_product_record` VALUES ('295', '57', '251', '', '964.0', '209', '2014-11-12 16:46:00');
INSERT INTO `fac_department_product_record` VALUES ('296', '62', '251', '', '948.0', '209', '2014-11-12 17:08:27');
INSERT INTO `fac_department_product_record` VALUES ('297', '45', '253', '', '2413.0', '212', '2014-11-14 15:26:40');
INSERT INTO `fac_department_product_record` VALUES ('298', '55', '253', '', '2228.0', '212', '2014-11-14 15:27:24');
INSERT INTO `fac_department_product_record` VALUES ('299', '45', '257', '', '1212.0', '217', '2014-11-20 10:47:34');
INSERT INTO `fac_department_product_record` VALUES ('301', '45', '256', '', '3557.0', '215', '2014-11-20 10:56:24');
INSERT INTO `fac_department_product_record` VALUES ('302', '45', '255', '', '1263.0', '214', '2014-11-20 10:57:12');
INSERT INTO `fac_department_product_record` VALUES ('303', '45', '254', '', '2414.0', '213', '2014-11-20 10:58:04');
INSERT INTO `fac_department_product_record` VALUES ('304', '45', '257', '', '1212.0', '216', '2014-11-20 10:58:52');
INSERT INTO `fac_department_product_record` VALUES ('305', '57', '253', '', '2000.0', '212', '2014-11-24 11:05:42');
INSERT INTO `fac_department_product_record` VALUES ('306', '45', '259', '', '2424.0', '219', '2014-11-27 14:57:17');
INSERT INTO `fac_department_product_record` VALUES ('307', '45', '258', '', '2460.0', '218', '2014-11-27 14:58:07');
INSERT INTO `fac_department_product_record` VALUES ('308', '45', '233', '', '2187.0', '221', '2014-11-27 15:01:54');
INSERT INTO `fac_department_product_record` VALUES ('309', '45', '266', '', '2402.0', '228', '2014-11-27 15:02:46');
INSERT INTO `fac_department_product_record` VALUES ('311', '45', '261', '', '1204.0', '229', '2014-11-27 15:05:17');
INSERT INTO `fac_department_product_record` VALUES ('312', '45', '262', '', '1201.0', '224', '2014-11-27 15:05:57');
INSERT INTO `fac_department_product_record` VALUES ('313', '45', '264', '', '2389.0', '226', '2014-11-27 15:06:48');
INSERT INTO `fac_department_product_record` VALUES ('314', '45', '272', '', '567.0', '238', '2014-11-27 15:07:38');
INSERT INTO `fac_department_product_record` VALUES ('316', '45', '265', '', '2407.0', '227', '2014-11-27 15:12:20');
INSERT INTO `fac_department_product_record` VALUES ('317', '55', '259', '', '2333.0', '219', '2014-11-27 15:32:21');
INSERT INTO `fac_department_product_record` VALUES ('319', '55', '258', '', '2368.0', '218', '2014-11-27 15:35:25');
INSERT INTO `fac_department_product_record` VALUES ('320', '55', '266', '', '2320.0', '228', '2014-11-27 15:37:03');
INSERT INTO `fac_department_product_record` VALUES ('321', '55', '272', '', '555.0', '238', '2014-11-27 15:38:44');
INSERT INTO `fac_department_product_record` VALUES ('322', '45', '237', '', '2404.0', '222', '2014-11-28 09:40:51');
INSERT INTO `fac_department_product_record` VALUES ('323', '45', '271', '', '1537.0', '237', '2014-11-28 09:41:57');
INSERT INTO `fac_department_product_record` VALUES ('324', '45', '265', '', '1323.0', '243', '2014-11-28 09:43:09');
INSERT INTO `fac_department_product_record` VALUES ('325', '45', '237', '', '1202.0', '242', '2014-11-28 09:43:57');
INSERT INTO `fac_department_product_record` VALUES ('326', '45', '255', '', '1271.0', '241', '2014-11-28 09:45:58');

-- ----------------------------
-- Table structure for fac_department_receive_record
-- ----------------------------
DROP TABLE IF EXISTS `fac_department_receive_record`;
CREATE TABLE `fac_department_receive_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `from_department_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `wnumber` varchar(255) COLLATE utf8_bin DEFAULT '',
  `number` float(11,1) DEFAULT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fac_department_receive_record_ibfk_1` (`department_id`) USING BTREE,
  KEY `fac_department_receive_record_ibfk_2` (`product_id`) USING BTREE,
  KEY `fac_department_receive_record_ibfk_3` (`from_department_id`) USING BTREE,
  CONSTRAINT `fac_department_receive_record_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `fac_department` (`department_id`),
  CONSTRAINT `fac_department_receive_record_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_department_receive_record
-- ----------------------------
INSERT INTO `fac_department_receive_record` VALUES ('171', '60', '53', '1', '', '1060.0', '186', '2014-10-12 12:53:47');
INSERT INTO `fac_department_receive_record` VALUES ('197', '58', '57', '231', '', '1100.0', '200', '2014-10-15 22:29:47');
INSERT INTO `fac_department_receive_record` VALUES ('198', '58', '57', '231', '', '1100.0', '194', '2014-10-15 22:30:32');
INSERT INTO `fac_department_receive_record` VALUES ('199', '59', '58', '231', '', '1100.0', '200', '2014-10-15 22:45:31');
INSERT INTO `fac_department_receive_record` VALUES ('200', '59', '58', '231', '', '1100.0', '194', '2014-10-15 22:46:16');
INSERT INTO `fac_department_receive_record` VALUES ('201', '53', '59', '231', '', '1080.0', '200', '2014-10-15 22:47:08');
INSERT INTO `fac_department_receive_record` VALUES ('202', '53', '59', '231', '', '1080.0', '194', '2014-10-15 22:47:30');
INSERT INTO `fac_department_receive_record` VALUES ('203', '61', '53', '231', '', '1050.0', '200', '2014-10-15 22:48:11');
INSERT INTO `fac_department_receive_record` VALUES ('204', '61', '53', '231', '', '1050.0', '194', '2014-10-15 22:48:17');
INSERT INTO `fac_department_receive_record` VALUES ('208', '45', '0', '94', '', '5.0', '201', '2014-10-20 15:19:12');
INSERT INTO `fac_department_receive_record` VALUES ('209', '55', '45', '94', '', '600.0', '201', '2014-10-20 15:21:54');
INSERT INTO `fac_department_receive_record` VALUES ('210', '57', '55', '94', '', '580.0', '201', '2014-10-20 15:27:50');
INSERT INTO `fac_department_receive_record` VALUES ('211', '45', '0', '248', '001', '10.0', '205', '2014-10-27 23:17:50');
INSERT INTO `fac_department_receive_record` VALUES ('213', '55', '45', '248', '002', '700.0', '205', '2014-10-27 23:28:59');
INSERT INTO `fac_department_receive_record` VALUES ('214', '55', '45', '248', '002', '500.0', '205', '2014-10-27 23:30:15');
INSERT INTO `fac_department_receive_record` VALUES ('215', '57', '55', '248', '003', '1100.0', '205', '2014-10-27 23:32:29');
INSERT INTO `fac_department_receive_record` VALUES ('216', '45', '0', '248', '8888', '10.0', '206', '2014-10-30 00:19:39');
INSERT INTO `fac_department_receive_record` VALUES ('217', '55', '45', '248', '9999', '500.0', '206', '2014-10-30 00:21:23');
INSERT INTO `fac_department_receive_record` VALUES ('218', '55', '45', '248', '9999', '700.0', '206', '2014-10-30 00:22:17');
INSERT INTO `fac_department_receive_record` VALUES ('219', '57', '55', '248', '888', '1130.0', '206', '2014-10-30 00:24:45');
INSERT INTO `fac_department_receive_record` VALUES ('220', '45', '0', '94', '001', '10.0', '207', '2014-10-31 02:53:34');
INSERT INTO `fac_department_receive_record` VALUES ('221', '55', '45', '94', '', '500.0', '207', '2014-10-31 02:54:12');
INSERT INTO `fac_department_receive_record` VALUES ('222', '55', '45', '94', '', '700.0', '207', '2014-10-31 02:54:38');
INSERT INTO `fac_department_receive_record` VALUES ('223', '57', '55', '94', '', '1200.0', '207', '2014-10-31 02:55:29');
INSERT INTO `fac_department_receive_record` VALUES ('224', '57', '55', '94', '001', '200.0', '207', '2014-11-04 13:49:53');
INSERT INTO `fac_department_receive_record` VALUES ('225', '55', '45', '248', '', '1200.0', '208', '2014-11-07 20:14:16');
INSERT INTO `fac_department_receive_record` VALUES ('226', '45', '0', '248', '', '10.0', '208', '2014-11-07 20:15:11');
INSERT INTO `fac_department_receive_record` VALUES ('227', '57', '55', '248', '', '1150.0', '208', '2014-11-07 20:15:53');
INSERT INTO `fac_department_receive_record` VALUES ('228', '45', '0', '251', '', '8.0', '209', '2014-11-08 22:54:44');
INSERT INTO `fac_department_receive_record` VALUES ('229', '55', '45', '251', '', '998.0', '209', '2014-11-11 14:34:06');
INSERT INTO `fac_department_receive_record` VALUES ('230', '57', '55', '251', '', '998.0', '209', '2014-11-11 14:34:39');
INSERT INTO `fac_department_receive_record` VALUES ('231', '62', '57', '251', '', '964.0', '209', '2014-11-12 17:08:07');
INSERT INTO `fac_department_receive_record` VALUES ('233', '45', '0', '253', '', '10.0', '212', '2014-11-14 15:26:25');
INSERT INTO `fac_department_receive_record` VALUES ('234', '55', '45', '253', '', '2413.0', '212', '2014-11-14 15:27:12');
INSERT INTO `fac_department_receive_record` VALUES ('235', '45', '0', '257', '', '9.0', '217', '2014-11-20 10:47:22');
INSERT INTO `fac_department_receive_record` VALUES ('236', '45', '0', '236', '', '40.0', '216', '2014-11-20 10:54:50');
INSERT INTO `fac_department_receive_record` VALUES ('237', '45', '0', '256', '', '25.0', '215', '2014-11-20 10:56:00');
INSERT INTO `fac_department_receive_record` VALUES ('238', '45', '0', '255', '', '8.0', '214', '2014-11-20 10:56:59');
INSERT INTO `fac_department_receive_record` VALUES ('239', '45', '0', '254', '', '12.0', '213', '2014-11-20 10:57:45');
INSERT INTO `fac_department_receive_record` VALUES ('240', '57', '55', '253', '', '2228.0', '212', '2014-11-24 11:05:21');
INSERT INTO `fac_department_receive_record` VALUES ('241', '45', '0', '259', '', '13.0', '219', '2014-11-27 14:56:53');
INSERT INTO `fac_department_receive_record` VALUES ('242', '45', '0', '258', '', '16.0', '218', '2014-11-27 14:57:44');
INSERT INTO `fac_department_receive_record` VALUES ('244', '45', '0', '233', '', '15.0', '221', '2014-11-27 15:01:51');
INSERT INTO `fac_department_receive_record` VALUES ('245', '45', '0', '266', '', '15.0', '228', '2014-11-27 15:02:22');
INSERT INTO `fac_department_receive_record` VALUES ('246', '45', '0', '265', '', '20.0', '227', '2014-11-27 15:03:52');
INSERT INTO `fac_department_receive_record` VALUES ('247', '45', '0', '261', '', '11.0', '229', '2014-11-27 15:05:13');
INSERT INTO `fac_department_receive_record` VALUES ('248', '45', '0', '262', '', '8.0', '224', '2014-11-27 15:05:55');
INSERT INTO `fac_department_receive_record` VALUES ('249', '45', '0', '264', '', '13.0', '226', '2014-11-27 15:06:45');
INSERT INTO `fac_department_receive_record` VALUES ('250', '45', '0', '272', '', '4.0', '238', '2014-11-27 15:07:32');
INSERT INTO `fac_department_receive_record` VALUES ('251', '55', '45', '259', '', '2424.0', '219', '2014-11-27 15:32:19');
INSERT INTO `fac_department_receive_record` VALUES ('252', '55', '45', '258', '', '2460.0', '218', '2014-11-27 15:35:28');
INSERT INTO `fac_department_receive_record` VALUES ('253', '55', '45', '266', '', '2402.0', '228', '2014-11-27 15:37:01');
INSERT INTO `fac_department_receive_record` VALUES ('255', '55', '45', '272', '', '567.0', '238', '2014-11-27 15:39:20');
INSERT INTO `fac_department_receive_record` VALUES ('256', '45', '0', '237', '', '17.0', '222', '2014-11-28 09:40:49');
INSERT INTO `fac_department_receive_record` VALUES ('257', '45', '0', '271', '', '12.0', '237', '2014-11-28 09:41:43');
INSERT INTO `fac_department_receive_record` VALUES ('258', '45', '0', '265', '', '10.0', '243', '2014-11-28 09:43:06');
INSERT INTO `fac_department_receive_record` VALUES ('259', '45', '0', '237', '', '8.0', '242', '2014-11-28 09:43:44');
INSERT INTO `fac_department_receive_record` VALUES ('260', '45', '0', '255', '', '8.0', '241', '2014-11-28 09:45:49');

-- ----------------------------
-- Table structure for fac_department_storehouse
-- ----------------------------
DROP TABLE IF EXISTS `fac_department_storehouse`;
CREATE TABLE `fac_department_storehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `finish_number` float(11,1) DEFAULT '0.0',
  `unfinish_number` float(11,1) DEFAULT '0.0',
  PRIMARY KEY (`id`),
  KEY `fac_department_record_ibfk_1` (`department_id`) USING BTREE,
  KEY `fac_department_record_ibfk_2` (`product_id`) USING BTREE,
  CONSTRAINT `fac_department_storehouse_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `fac_department` (`department_id`),
  CONSTRAINT `fac_department_storehouse_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_department_storehouse
-- ----------------------------
INSERT INTO `fac_department_storehouse` VALUES ('51', '55', '93', '2150.0', '-48.0');
INSERT INTO `fac_department_storehouse` VALUES ('52', '45', '93', '1080.0', '1190.0');
INSERT INTO `fac_department_storehouse` VALUES ('53', '45', '3', '0.0', '2360.5');
INSERT INTO `fac_department_storehouse` VALUES ('54', '55', '1', '0.0', '-100.0');
INSERT INTO `fac_department_storehouse` VALUES ('55', '55', '3', '800.0', '250.0');
INSERT INTO `fac_department_storehouse` VALUES ('56', '53', '93', '-1.0', '-20.0');
INSERT INTO `fac_department_storehouse` VALUES ('57', '53', '3', '0.0', '-120.0');
INSERT INTO `fac_department_storehouse` VALUES ('58', '57', '93', '1120.0', '-30.0');
INSERT INTO `fac_department_storehouse` VALUES ('59', '58', '93', '1100.0', '-20.0');
INSERT INTO `fac_department_storehouse` VALUES ('60', '59', '93', '0.0', '-20.0');
INSERT INTO `fac_department_storehouse` VALUES ('61', '60', '1', '0.0', '1060.0');
INSERT INTO `fac_department_storehouse` VALUES ('62', '60', '93', '1050.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('63', '45', '1', '150.0', '297.5');
INSERT INTO `fac_department_storehouse` VALUES ('64', '57', '1', '0.0', '-50.0');
INSERT INTO `fac_department_storehouse` VALUES ('65', '58', '1', '0.0', '-10.0');
INSERT INTO `fac_department_storehouse` VALUES ('66', '59', '1', '0.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('67', '61', '93', '0.0', '-30.0');
INSERT INTO `fac_department_storehouse` VALUES ('68', '45', '231', '1200.0', '2380.0');
INSERT INTO `fac_department_storehouse` VALUES ('69', '45', '232', '320.0', '1185.0');
INSERT INTO `fac_department_storehouse` VALUES ('70', '55', '232', '0.0', '-70.0');
INSERT INTO `fac_department_storehouse` VALUES ('71', '45', '233', '3387.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('72', '45', '234', '1200.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('73', '45', '236', '1200.0', '3640.0');
INSERT INTO `fac_department_storehouse` VALUES ('74', '45', '237', '3606.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('75', '55', '231', '500.0', '-100.0');
INSERT INTO `fac_department_storehouse` VALUES ('76', '55', '233', '0.0', '-50.0');
INSERT INTO `fac_department_storehouse` VALUES ('77', '55', '234', '0.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('78', '55', '236', '0.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('79', '57', '94', '550.0', '1400.0');
INSERT INTO `fac_department_storehouse` VALUES ('80', '57', '231', '-1100.0', '-100.0');
INSERT INTO `fac_department_storehouse` VALUES ('81', '58', '231', '1100.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('82', '59', '231', '1080.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('83', '53', '231', '1050.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('84', '61', '231', '2000.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('85', '45', '94', '100.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('86', '55', '94', '-200.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('87', '45', '248', '500.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('88', '55', '248', '1100.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('89', '57', '248', '2150.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('90', '45', '251', '998.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('91', '55', '251', '998.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('92', '57', '251', '964.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('93', '62', '251', '948.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('94', '45', '253', '2413.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('95', '55', '253', '2228.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('96', '45', '257', '2424.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('97', '45', '256', '3557.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('98', '45', '255', '2534.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('99', '45', '254', '2414.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('100', '57', '253', '2000.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('101', '45', '259', '2424.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('102', '45', '258', '0.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('103', '45', '206', '0.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('104', '45', '266', '2402.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('105', '45', '265', '3730.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('106', '45', '261', '1204.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('107', '45', '262', '1201.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('108', '45', '264', '2389.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('109', '45', '272', '112712.0', '567.0');
INSERT INTO `fac_department_storehouse` VALUES ('110', '55', '259', '2333.0', '2368.0');
INSERT INTO `fac_department_storehouse` VALUES ('111', '55', '258', '2368.0', '2460.0');
INSERT INTO `fac_department_storehouse` VALUES ('112', '55', '266', '2320.0', '0.0');
INSERT INTO `fac_department_storehouse` VALUES ('113', '55', '272', '555.0', '-112145.0');
INSERT INTO `fac_department_storehouse` VALUES ('114', '45', '271', '1537.0', '0.0');

-- ----------------------------
-- Table structure for fac_flow
-- ----------------------------
DROP TABLE IF EXISTS `fac_flow`;
CREATE TABLE `fac_flow` (
  `flow_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流程Id',
  `flow_name` varchar(20) COLLATE utf8_bin NOT NULL COMMENT '流程名字',
  `hidden` enum('no','yes') COLLATE utf8_bin NOT NULL DEFAULT 'no' COMMENT '删除标记状态',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_flow
-- ----------------------------
INSERT INTO `fac_flow` VALUES ('54', '5s流程', 'no');

-- ----------------------------
-- Table structure for fac_flow_mess
-- ----------------------------
DROP TABLE IF EXISTS `fac_flow_mess`;
CREATE TABLE `fac_flow_mess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flow_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `is_firs` tinyint(4) DEFAULT '0',
  `is_last` tinyint(4) DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fac_flow_mess_ibfk_1` (`flow_id`) USING BTREE,
  KEY `fac_flow_mess_ibfk_2` (`department_id`) USING BTREE,
  CONSTRAINT `fac_flow_mess_ibfk_1` FOREIGN KEY (`flow_id`) REFERENCES `fac_flow` (`flow_id`),
  CONSTRAINT `fac_flow_mess_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `fac_department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1059 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_flow_mess
-- ----------------------------
INSERT INTO `fac_flow_mess` VALUES ('1053', '54', '42', '0', '0', '1');
INSERT INTO `fac_flow_mess` VALUES ('1054', '54', '46', '0', '0', '2');
INSERT INTO `fac_flow_mess` VALUES ('1055', '54', '47', '0', '0', '3');
INSERT INTO `fac_flow_mess` VALUES ('1056', '54', '43', '0', '0', '4');
INSERT INTO `fac_flow_mess` VALUES ('1057', '54', '53', '0', '0', '5');
INSERT INTO `fac_flow_mess` VALUES ('1058', '54', '44', '0', '0', '6');

-- ----------------------------
-- Table structure for fac_give_material_record
-- ----------------------------
DROP TABLE IF EXISTS `fac_give_material_record`;
CREATE TABLE `fac_give_material_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `number` float(11,0) DEFAULT NULL,
  `give_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_give_material_record
-- ----------------------------
INSERT INTO `fac_give_material_record` VALUES ('1', '45', '92', '10', '2014-10-12 19:24:22');
INSERT INTO `fac_give_material_record` VALUES ('2', '57', '91', '1', '2014-10-12 19:39:21');
INSERT INTO `fac_give_material_record` VALUES ('3', '57', '91', '1', '2014-10-26 03:05:59');

-- ----------------------------
-- Table structure for fac_group
-- ----------------------------
DROP TABLE IF EXISTS `fac_group`;
CREATE TABLE `fac_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_group
-- ----------------------------
INSERT INTO `fac_group` VALUES ('1', '默认分组');

-- ----------------------------
-- Table structure for fac_material_demand
-- ----------------------------
DROP TABLE IF EXISTS `fac_material_demand`;
CREATE TABLE `fac_material_demand` (
  `material_demand_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) DEFAULT NULL,
  `material_demand_code` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `demand_date` date DEFAULT NULL,
  `apply` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  `order_state` enum('已采购','待审核','未通过审核','通过审核') COLLATE utf8_bin DEFAULT NULL,
  `price` int(11) DEFAULT '0',
  PRIMARY KEY (`material_demand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_material_demand
-- ----------------------------

-- ----------------------------
-- Table structure for fac_material_demand_item
-- ----------------------------
DROP TABLE IF EXISTS `fac_material_demand_item`;
CREATE TABLE `fac_material_demand_item` (
  `material_demand_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_demand_id` int(11) DEFAULT NULL,
  `material_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `unit_price` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`material_demand_item_id`),
  KEY `material_demand_id` (`material_demand_id`) USING BTREE,
  KEY `material_id` (`material_id`) USING BTREE,
  CONSTRAINT `fac_material_demand_item_ibfk_1` FOREIGN KEY (`material_demand_id`) REFERENCES `fac_material_demand` (`material_demand_id`),
  CONSTRAINT `fac_material_demand_item_ibfk_2` FOREIGN KEY (`material_id`) REFERENCES `fac_material_mess` (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_material_demand_item
-- ----------------------------

-- ----------------------------
-- Table structure for fac_material_mess
-- ----------------------------
DROP TABLE IF EXISTS `fac_material_mess`;
CREATE TABLE `fac_material_mess` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_code` varchar(20) DEFAULT NULL,
  `material_name` varchar(20) DEFAULT NULL,
  `material_type` varchar(20) DEFAULT NULL COMMENT '规格',
  `number` int(20) DEFAULT NULL,
  `measure` varchar(11) DEFAULT NULL,
  `remark` varchar(40) DEFAULT NULL,
  `hidden` enum('yes','no') DEFAULT 'no' COMMENT '执行删除时隐藏',
  PRIMARY KEY (`material_id`),
  UNIQUE KEY `uniname` (`material_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_material_mess
-- ----------------------------
INSERT INTO `fac_material_mess` VALUES ('1', '001', '玻璃0.2', '0.2', '1000', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('55', '004', 'AB胶', '0.3', '100', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('89', '002', '玻璃0.3', '0.3', '2000', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('90', '005', '防指纹油', '1KG/瓶', '5022', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('91', '006', '扫光粉', '0.44', '497', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('92', '003', '玻璃0.4', '0.4', '990', null, '', 'no');
INSERT INTO `fac_material_mess` VALUES ('93', '007', '切削液', '20KG/桶', '104', null, '', 'no');

-- ----------------------------
-- Table structure for fac_material_record
-- ----------------------------
DROP TABLE IF EXISTS `fac_material_record`;
CREATE TABLE `fac_material_record` (
  `trade_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) DEFAULT NULL,
  `operator` varchar(11) DEFAULT NULL,
  `price` float(11,2) DEFAULT NULL,
  `trade_date` datetime DEFAULT NULL,
  `trade_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`trade_id`),
  KEY `material_id` (`material_id`) USING BTREE,
  CONSTRAINT `fac_material_record_ibfk_1` FOREIGN KEY (`material_id`) REFERENCES `fac_material_mess` (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`material_id`) REFER `assembly_line_system/fac_material_m';

-- ----------------------------
-- Records of fac_material_record
-- ----------------------------
INSERT INTO `fac_material_record` VALUES ('1', '90', '111', '10.00', '2014-10-12 23:29:48', '1000');
INSERT INTO `fac_material_record` VALUES ('2', '90', '111', '10.00', '2014-10-12 23:30:19', '1000');
INSERT INTO `fac_material_record` VALUES ('3', '89', '111', '1.00', '2014-10-12 23:30:40', '1000');

-- ----------------------------
-- Table structure for fac_order_department_mess
-- ----------------------------
DROP TABLE IF EXISTS `fac_order_department_mess`;
CREATE TABLE `fac_order_department_mess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_order_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `sort` int(5) DEFAULT NULL,
  `product_sum` int(11) DEFAULT NULL,
  `taskstate` enum('未开始','已完成','生产中','已暂停') DEFAULT NULL,
  `product_yes` int(11) DEFAULT NULL,
  `product_no` int(11) DEFAULT NULL,
  `product_wait` int(11) DEFAULT NULL,
  `worktime` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fac_order_department_mess_ibfk_1` (`product_order_id`) USING BTREE,
  CONSTRAINT `fac_order_department_mess_ibfk_1` FOREIGN KEY (`product_order_id`) REFERENCES `fac_product_order` (`product_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_order_department_mess
-- ----------------------------

-- ----------------------------
-- Table structure for fac_product_mess
-- ----------------------------
DROP TABLE IF EXISTS `fac_product_mess`;
CREATE TABLE `fac_product_mess` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(30) DEFAULT NULL,
  `product_model` varchar(20) DEFAULT NULL,
  `product_material` varchar(20) DEFAULT NULL,
  `gongyi` varchar(11) DEFAULT NULL,
  `number` int(20) DEFAULT '0',
  `hidden` enum('no','yes') DEFAULT 'no' COMMENT '执行删除时，若外键限制则隐藏',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_product_mess
-- ----------------------------
INSERT INTO `fac_product_mess` VALUES ('1', 'A1002', 'I4', '', '曲边0.44', '0', 'yes');
INSERT INTO `fac_product_mess` VALUES ('3', 'A1003', 'I5', '', '曲边0.33', '0', 'yes');
INSERT INTO `fac_product_mess` VALUES ('93', 'A1005', 'I6', '', '曲边0.34', '1', 'yes');
INSERT INTO `fac_product_mess` VALUES ('94', 'A01', 'I4', '', '0.4直边', '25', 'no');
INSERT INTO `fac_product_mess` VALUES ('95', 'A02', 'I5后盖', '', '0.4直边', '2361', 'no');
INSERT INTO `fac_product_mess` VALUES ('96', 'B01', 'S4', '', '0.4直边', '519', 'no');
INSERT INTO `fac_product_mess` VALUES ('97', 'B02', 'S4mini', '', '0.4直边', '1861', 'no');
INSERT INTO `fac_product_mess` VALUES ('98', 'B03', '9300', '', '0.4直边', '32', 'no');
INSERT INTO `fac_product_mess` VALUES ('99', 'B04', 'S3mini', '', '0.4直边', '2509', 'no');
INSERT INTO `fac_product_mess` VALUES ('100', 'B05', 'S5', '', '0.4直边', '75', 'no');
INSERT INTO `fac_product_mess` VALUES ('101', 'B06', '7106', '', '0.4直边', '1628', 'no');
INSERT INTO `fac_product_mess` VALUES ('102', 'B07', '7262', '', '0.4直边', '1473', 'no');
INSERT INTO `fac_product_mess` VALUES ('103', 'B08', '8262', '', '0.4直边', '589', 'no');
INSERT INTO `fac_product_mess` VALUES ('104', 'B09', '8552', '', '0.4直边', '1388', 'no');
INSERT INTO `fac_product_mess` VALUES ('105', 'B10', '9100', '', '0.4直边', '75', 'no');
INSERT INTO `fac_product_mess` VALUES ('106', 'B11', '9150', '', '0.4直边', '20', 'no');
INSERT INTO `fac_product_mess` VALUES ('107', 'B12', '9082', '', '0.4直边', '11', 'no');
INSERT INTO `fac_product_mess` VALUES ('109', 'B', 'note3B', '', '0.4直边', '1248', 'no');
INSERT INTO `fac_product_mess` VALUES ('110', 'B', 'note4', '', '0.4直边', '26', 'no');
INSERT INTO `fac_product_mess` VALUES ('111', 'B', 'note4B', '', '0.4直边', '722', 'no');
INSERT INTO `fac_product_mess` VALUES ('112', 'B', 'note3mini', '', '0.4直边', '197', 'no');
INSERT INTO `fac_product_mess` VALUES ('113', 'B', 'note3one', '', '0.4直边', '628', 'no');
INSERT INTO `fac_product_mess` VALUES ('114', 'C', '红米', '', '0.4直边', '60', 'no');
INSERT INTO `fac_product_mess` VALUES ('115', 'C', '米2', '', '0.4直边', '195', 'no');
INSERT INTO `fac_product_mess` VALUES ('116', 'C', '米3', '', '0.4直边', '37', 'no');
INSERT INTO `fac_product_mess` VALUES ('117', 'D', 'M7', '', '0.4直边', '757', 'no');
INSERT INTO `fac_product_mess` VALUES ('118', 'D', 'M8', '', '0.4直边', '1678', 'no');
INSERT INTO `fac_product_mess` VALUES ('119', 'B', 'Tabs8.4', '', '0.4直边', '629', 'no');
INSERT INTO `fac_product_mess` VALUES ('120', 'B', 'T231', '', '0.4直边', '422', 'no');
INSERT INTO `fac_product_mess` VALUES ('121', 'D', 'HTC816', '', '0.4直边', '4', 'no');
INSERT INTO `fac_product_mess` VALUES ('122', 'E', 'Z1', '', '0.4直边', '177', 'no');
INSERT INTO `fac_product_mess` VALUES ('123', 'E', 'Z2', '', '0.4直边', '114', 'no');
INSERT INTO `fac_product_mess` VALUES ('124', 'E', 'Z4', '', '0.4直边', '12', 'no');
INSERT INTO `fac_product_mess` VALUES ('125', 'E', 'Z5', '', '0.4直边', '122', 'no');
INSERT INTO `fac_product_mess` VALUES ('126', 'E', 'Z6', '', '0.4直边', '622', 'no');
INSERT INTO `fac_product_mess` VALUES ('127', 'F', '3C', '', '0.4直边', '27', 'no');
INSERT INTO `fac_product_mess` VALUES ('128', 'F', 'P6', '', '0.4直边', '19', 'no');
INSERT INTO `fac_product_mess` VALUES ('129', 'F', 'P7', '', '0.4直边', '25', 'no');
INSERT INTO `fac_product_mess` VALUES ('130', 'J', 'motoG2', '', '0.4直边', '1290', 'no');
INSERT INTO `fac_product_mess` VALUES ('131', 'J', 'motoG', '', '0.4直边', '1930', 'no');
INSERT INTO `fac_product_mess` VALUES ('132', 'J', 'motoX', '', '0.4直边', '314', 'no');
INSERT INTO `fac_product_mess` VALUES ('133', 'J', 'motoE', '', '0.4直边', '811', 'no');
INSERT INTO `fac_product_mess` VALUES ('134', 'I', 'G2', '', '0.4直边', '767', 'no');
INSERT INTO `fac_product_mess` VALUES ('135', 'I', 'G3', '', '0.4直边', '3233', 'no');
INSERT INTO `fac_product_mess` VALUES ('136', 'I', 'G2mini', '', '0.4直边', '686', 'no');
INSERT INTO `fac_product_mess` VALUES ('137', 'I', 'G313', '', '0.4直边', '719', 'no');
INSERT INTO `fac_product_mess` VALUES ('138', 'A', 'Ipad2', '', '0.4直边', '39', 'no');
INSERT INTO `fac_product_mess` VALUES ('139', 'A', 'Ipad5', '', '0.4直边', '107', 'no');
INSERT INTO `fac_product_mess` VALUES ('140', 'A', 'Ipadmini', '', '0.4直边', '8', 'no');
INSERT INTO `fac_product_mess` VALUES ('141', 'A', 'I4后盖', '', '0.4弧边', '352', 'no');
INSERT INTO `fac_product_mess` VALUES ('142', 'A', 'I5', '', '0.4弧边', '25', 'no');
INSERT INTO `fac_product_mess` VALUES ('143', 'A', 'I5后盖', '', '0.4弧边', '31', 'no');
INSERT INTO `fac_product_mess` VALUES ('144', 'B', '3812', '', '0.4弧边', '124', 'no');
INSERT INTO `fac_product_mess` VALUES ('145', 'B', '7100', '', '0.4弧边', '19', 'no');
INSERT INTO `fac_product_mess` VALUES ('146', 'B', '7106', '', '0.4弧边', '232', 'no');
INSERT INTO `fac_product_mess` VALUES ('147', 'B', '0.4弧边', '', '7505', '10', 'no');
INSERT INTO `fac_product_mess` VALUES ('148', 'B', '7562', '', '0.4弧边', '49', 'no');
INSERT INTO `fac_product_mess` VALUES ('149', 'B', '8262', '', '0.4弧边', '6', 'no');
INSERT INTO `fac_product_mess` VALUES ('150', 'B', '816', '', '0.4弧边', '1605', 'no');
INSERT INTO `fac_product_mess` VALUES ('151', 'B', '9100', '', '0.4弧边', '513', 'no');
INSERT INTO `fac_product_mess` VALUES ('152', 'B', '9200', '', '0.4弧边', '27', 'no');
INSERT INTO `fac_product_mess` VALUES ('153', 'B', '9300', '', '0.4弧边', '387', 'no');
INSERT INTO `fac_product_mess` VALUES ('154', 'B', '9082', '', '0.4弧边', '683', 'no');
INSERT INTO `fac_product_mess` VALUES ('155', 'B', 'Note4', '', '0.4弧边', '609', 'no');
INSERT INTO `fac_product_mess` VALUES ('156', 'B', 'Note3B', '', '0.4弧边', '8', 'no');
INSERT INTO `fac_product_mess` VALUES ('157', 'B', 'Note3one', '', '0.4弧边', '1390', 'no');
INSERT INTO `fac_product_mess` VALUES ('158', 'B', 'Note3mini', '', '0.4弧边', '74', 'no');
INSERT INTO `fac_product_mess` VALUES ('159', 'B', 'G3556', '', '0.4弧边', '1968', 'no');
INSERT INTO `fac_product_mess` VALUES ('160', 'B', 'S5', '', '0.4弧边', '3201', 'no');
INSERT INTO `fac_product_mess` VALUES ('161', 'B', 'S3mini', '', '0.4弧边', '997', 'no');
INSERT INTO `fac_product_mess` VALUES ('162', 'B', 'S4mini', '', '0.4弧边', '473', 'no');
INSERT INTO `fac_product_mess` VALUES ('163', 'C', '红米note', '', '0.4弧边', '115', 'no');
INSERT INTO `fac_product_mess` VALUES ('164', 'C', '米3', '', '0.4弧边', '162', 'no');
INSERT INTO `fac_product_mess` VALUES ('165', 'D', 'M7', '', '0.4弧边', '1204', 'no');
INSERT INTO `fac_product_mess` VALUES ('166', 'D', 'M8', '', '0.4弧边', '36', 'no');
INSERT INTO `fac_product_mess` VALUES ('167', 'E', 'Z', '', '0.4弧边', '175', 'no');
INSERT INTO `fac_product_mess` VALUES ('168', 'E', 'Z2', '', '0.4弧边', '1355', 'no');
INSERT INTO `fac_product_mess` VALUES ('169', 'E', 'Z4', '', '0.4弧边', '1781', 'no');
INSERT INTO `fac_product_mess` VALUES ('170', 'E', 'Z5', '', '0.4弧边', '851', 'no');
INSERT INTO `fac_product_mess` VALUES ('171', 'E', 'Z6', '', '0.4弧边', '16', 'no');
INSERT INTO `fac_product_mess` VALUES ('172', 'G', 'MX3', '', '0.4弧边', '24', 'no');
INSERT INTO `fac_product_mess` VALUES ('173', 'J', 'MotoG', '', '0.4弧边', '690', 'no');
INSERT INTO `fac_product_mess` VALUES ('174', 'J', 'MotoE', '', '0.4弧边', '253', 'no');
INSERT INTO `fac_product_mess` VALUES ('175', 'J', 'MotoX', '', '0.4弧边', '2415', 'no');
INSERT INTO `fac_product_mess` VALUES ('176', 'I', 'G2', '', '0.4弧边', '595', 'no');
INSERT INTO `fac_product_mess` VALUES ('177', 'I', 'G3', '', '0.4弧边', '106', 'no');
INSERT INTO `fac_product_mess` VALUES ('178', 'I', 'G2mini', '', '0.4弧边', '587', 'no');
INSERT INTO `fac_product_mess` VALUES ('179', 'A', 'I4', '', '0.33直边', '18', 'no');
INSERT INTO `fac_product_mess` VALUES ('180', 'B', 'S4', '', '0.33直边', '22', 'no');
INSERT INTO `fac_product_mess` VALUES ('181', 'B', 'Note3B', '', '0.33直边', '18', 'no');
INSERT INTO `fac_product_mess` VALUES ('182', 'B', '9300', '', '0.33直边', '100', 'no');
INSERT INTO `fac_product_mess` VALUES ('183', 'D', 'HTC816', '', '0.33直边', '207', 'no');
INSERT INTO `fac_product_mess` VALUES ('184', 'C', '米3', '', '0.33直边', '31', 'no');
INSERT INTO `fac_product_mess` VALUES ('185', 'B ', 'Tabs8.4', '', '0.33直边', '15', 'no');
INSERT INTO `fac_product_mess` VALUES ('186', 'E', 'Z', '', '0.33直边', '133', 'no');
INSERT INTO `fac_product_mess` VALUES ('187', 'F', 'P7', '', '0.33直边', '275', 'no');
INSERT INTO `fac_product_mess` VALUES ('188', 'G', 'MX3', '', '0.33直边', '3', 'no');
INSERT INTO `fac_product_mess` VALUES ('189', 'H', 'Z4', '', '0.33直边', '558', 'no');
INSERT INTO `fac_product_mess` VALUES ('190', 'H', 'Z5', '', '0.33直边', '188', 'no');
INSERT INTO `fac_product_mess` VALUES ('191', 'I', 'G2', '', '0.33直边', '200', 'no');
INSERT INTO `fac_product_mess` VALUES ('192', 'I', '谷5', '', '0.33直边', '530', 'no');
INSERT INTO `fac_product_mess` VALUES ('193', 'J', 'MotoX', '', '0.33直边', '180', 'no');
INSERT INTO `fac_product_mess` VALUES ('194', 'J', 'MotoE', '', '0.33直边', '109', 'no');
INSERT INTO `fac_product_mess` VALUES ('195', 'A', 'I4后盖', '', '0.33弧边', '270', 'no');
INSERT INTO `fac_product_mess` VALUES ('196', 'A', 'Ipadmini', '', '0.33弧边', '1049', 'no');
INSERT INTO `fac_product_mess` VALUES ('197', 'B', 'Note3one', '', '0.33弧边', '100', 'no');
INSERT INTO `fac_product_mess` VALUES ('198', 'B', 'Note3B', '', '0.33弧边', '975', 'no');
INSERT INTO `fac_product_mess` VALUES ('199', 'B', 'Note4', '', '0.33弧边', '75', 'no');
INSERT INTO `fac_product_mess` VALUES ('200', 'B', 'Note4B', '', '0.33弧边', '1532', 'no');
INSERT INTO `fac_product_mess` VALUES ('201', 'B', '7505', '', '0.33弧边', '762', 'no');
INSERT INTO `fac_product_mess` VALUES ('202', 'B', '9100', '', '0.33弧边', '125', 'no');
INSERT INTO `fac_product_mess` VALUES ('203', 'B', '9082', '', '0.33弧边', '1242', 'no');
INSERT INTO `fac_product_mess` VALUES ('204', 'B', '3518', '', '0.33弧边', '82', 'no');
INSERT INTO `fac_product_mess` VALUES ('205', 'B', '3812', '', '0.33弧边', '483', 'no');
INSERT INTO `fac_product_mess` VALUES ('206', 'B', 'S4', '', '0.33弧边', '13', 'no');
INSERT INTO `fac_product_mess` VALUES ('207', 'B', 'S5', '', '0.33弧边', '511', 'no');
INSERT INTO `fac_product_mess` VALUES ('208', 'B', 'S3mini', '', '0.33弧边', '95', 'no');
INSERT INTO `fac_product_mess` VALUES ('209', 'b', 'Tabs8.4', '', '0.33弧边', '59', 'no');
INSERT INTO `fac_product_mess` VALUES ('210', 'B', 'T231', '', '0.33弧边', '26', 'no');
INSERT INTO `fac_product_mess` VALUES ('211', 'C', '米2', '', '0.33弧边', '332', 'no');
INSERT INTO `fac_product_mess` VALUES ('212', 'C', '米3', '', '0.33弧边', '525', 'no');
INSERT INTO `fac_product_mess` VALUES ('213', 'D', 'HTC816', '', '0.33弧边', '53', 'no');
INSERT INTO `fac_product_mess` VALUES ('214', 'D', 'M7', '', '0.33弧边', '313', 'no');
INSERT INTO `fac_product_mess` VALUES ('215', 'E', 'Z1', '', '0.33弧边', '1228', 'no');
INSERT INTO `fac_product_mess` VALUES ('216', 'E', 'Z2', '', '0.33弧边', '1573', 'no');
INSERT INTO `fac_product_mess` VALUES ('217', 'E', 'Z3', '', '0.33弧边', '3', 'no');
INSERT INTO `fac_product_mess` VALUES ('218', 'E', 'Z', '', '0.33弧边', '1446', 'no');
INSERT INTO `fac_product_mess` VALUES ('219', 'F', '3C', '', '0.33弧边', '341', 'no');
INSERT INTO `fac_product_mess` VALUES ('220', 'H', 'Z4', '', '0.33弧边', '836', 'no');
INSERT INTO `fac_product_mess` VALUES ('221', 'H', 'Z5', '', '0.33弧边', '15', 'no');
INSERT INTO `fac_product_mess` VALUES ('222', 'G', 'MX2', '', '0.33弧边', '602', 'no');
INSERT INTO `fac_product_mess` VALUES ('223', 'G', 'MX3', '', '0.33弧边', '904', 'no');
INSERT INTO `fac_product_mess` VALUES ('224', 'I', 'G2', '', '0.33弧边', '454', 'no');
INSERT INTO `fac_product_mess` VALUES ('225', 'I', 'G3', '', '0.33弧边', '255', 'no');
INSERT INTO `fac_product_mess` VALUES ('226', 'I', 'G2mini', '', '0.33弧边', '106', 'no');
INSERT INTO `fac_product_mess` VALUES ('227', 'I', '谷5', '', '0.33弧边', '114', 'no');
INSERT INTO `fac_product_mess` VALUES ('228', 'J', 'MotoG2', '', '0.33弧边', '4121', 'no');
INSERT INTO `fac_product_mess` VALUES ('229', 'J', 'MotoG', '', '0.33弧边', '14', 'no');
INSERT INTO `fac_product_mess` VALUES ('230', 'K02', 'N630', '', '0.33弧边', '200', 'no');
INSERT INTO `fac_product_mess` VALUES ('231', '', 'i6D', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('232', '', 'i6 5.5B', '', '0.33直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('233', '', 'S4', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('234', '', 'S5', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('235', '', 'I6D', '', '0.2直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('236', '', 'MEGA2', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('237', '', '红米', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('238', '', 'I6E', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('239', '', 'I6 5.5B', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('240', '', '9100', '', '0.33弧', '50', 'no');
INSERT INTO `fac_product_mess` VALUES ('241', '', '9100', '', '0.33弧', '50', 'no');
INSERT INTO `fac_product_mess` VALUES ('242', '', 'I6', '', '0.33弧', '100', 'no');
INSERT INTO `fac_product_mess` VALUES ('244', '', '9100', '', '0.33弧', '50', 'no');
INSERT INTO `fac_product_mess` VALUES ('246', '', '9100', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('247', '', '9300', '', '510', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('248', '', 'I6E', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('249', '', 'Y600', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('250', '', 'I6', '', '0.33弧', '50', 'no');
INSERT INTO `fac_product_mess` VALUES ('251', '', '7106', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('252', '', 'S4', '', '0.4弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('253', '', 'S5', '', '0.4弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('254', '', 'I4', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('255', '', '9300', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('256', '', 'SONY M2', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('257', '', '华硕Z5', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('258', '', 'I6 4.7后', '', '0.4弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('259', '', 'G313', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('260', '', '7100', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('261', '', 'NOTE 3B', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('262', '', 'G3556', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('263', '', '9150', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('264', '', 'I5后盖', '', '0.4弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('265', '', 'NOTE 4', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('266', '', '9300', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('267', '', '米4', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('268', '', 'I5', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('269', '', 'Alpha', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('270', '', '红米Note', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('271', '', 'G530', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('272', '', 'G2A', '', '0.4直', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('273', '', 'MEGA', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('274', '', 'Z1', '', '0.33弧', '0', 'no');
INSERT INTO `fac_product_mess` VALUES ('275', '', '7562', '', '0.33弧', '0', 'no');

-- ----------------------------
-- Table structure for fac_product_order
-- ----------------------------
DROP TABLE IF EXISTS `fac_product_order`;
CREATE TABLE `fac_product_order` (
  `product_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_order_code` varchar(20) DEFAULT NULL,
  `flow_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `order_state` enum('已完成','未开始','生产中','未完成') DEFAULT '未完成',
  `finish_date` datetime DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `finish_number` int(11) DEFAULT '0',
  PRIMARY KEY (`product_order_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `fac_product_order_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`product_id`) REFER `assembly_line_system/fac_product_mes';

-- ----------------------------
-- Records of fac_product_order
-- ----------------------------
INSERT INTO `fac_product_order` VALUES ('218', '112501', '0', '258', '2014-11-24 17:15:43', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('219', '112516', '0', '259', '2014-11-24 17:16:06', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('220', '112602', '0', '260', '2014-11-25 18:20:23', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('221', '112603', '0', '233', '2014-11-25 18:21:25', null, '未完成', null, '2200', '', '0');
INSERT INTO `fac_product_order` VALUES ('222', '112605', '0', '237', '2014-11-25 18:22:18', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('224', '112612', '0', '262', '2014-11-25 18:23:42', null, '未完成', null, '1200', '', '0');
INSERT INTO `fac_product_order` VALUES ('225', '112613', '0', '263', '2014-11-25 18:24:14', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('226', '112615', '0', '264', '2014-11-25 18:24:57', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('227', '112620', '0', '265', '2014-11-25 18:25:46', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('228', '112609', '0', '266', '2014-11-25 18:54:57', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('229', '112606', '0', '261', '2014-11-26 08:34:09', null, '未完成', null, '1200', '', '0');
INSERT INTO `fac_product_order` VALUES ('230', '112701', '0', '267', '2014-11-26 14:19:06', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('231', '112708', '0', '268', '2014-11-26 14:19:40', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('232', '112716', '0', '269', '2014-11-26 14:20:09', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('233', '112719', '0', '270', '2014-11-26 14:20:44', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('234', '112706', '0', '261', '2014-11-26 21:50:11', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('235', '112707', '0', '248', '2014-11-26 21:50:38', null, '未完成', null, '6000', '', '0');
INSERT INTO `fac_product_order` VALUES ('236', '112704', '0', '251', '2014-11-27 01:59:24', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('237', '112711', '0', '271', '2014-11-27 02:00:16', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('238', '112712', '0', '272', '2014-11-27 02:00:40', null, '未完成', null, '600', '', '0');
INSERT INTO `fac_product_order` VALUES ('239', '112714', '0', '273', '2014-11-27 02:01:25', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('240', '112715', '0', '264', '2014-11-27 02:01:55', null, '未完成', null, '2400', '', '0');
INSERT INTO `fac_product_order` VALUES ('241', '112709', '0', '255', '2014-11-27 14:42:43', null, '未完成', null, '1200', '', '0');
INSERT INTO `fac_product_order` VALUES ('242', '112705', '0', '237', '2014-11-27 16:18:03', null, '未完成', null, '1200', '', '0');
INSERT INTO `fac_product_order` VALUES ('243', '112720', '0', '265', '2014-11-27 16:37:18', null, '未完成', null, '1200', '', '0');
INSERT INTO `fac_product_order` VALUES ('244', '112811', '0', '268', '2014-11-27 23:16:37', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('245', '112817', '0', '274', '2014-11-27 23:17:03', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('246', '112809', '0', '275', '2014-11-28 12:15:34', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('247', '112805', '0', '248', '2014-11-28 14:07:12', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('248', '112820', '0', '265', '2014-11-28 15:15:44', null, '未完成', null, '3600', '', '0');
INSERT INTO `fac_product_order` VALUES ('249', '112802', '0', '254', '2014-11-28 16:33:25', null, '未完成', null, '3600', '', '0');

-- ----------------------------
-- Table structure for fac_product_order_item
-- ----------------------------
DROP TABLE IF EXISTS `fac_product_order_item`;
CREATE TABLE `fac_product_order_item` (
  `product_order_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `product_order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_order_item_id`),
  KEY `material_id` (`material_id`) USING BTREE,
  KEY `product_order_id` (`product_order_id`) USING BTREE,
  CONSTRAINT `fac_product_order_item_ibfk_1` FOREIGN KEY (`material_id`) REFERENCES `fac_material_mess` (`material_id`),
  CONSTRAINT `fac_product_order_item_ibfk_2` FOREIGN KEY (`product_order_id`) REFERENCES `fac_product_order` (`product_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_product_order_item
-- ----------------------------

-- ----------------------------
-- Table structure for fac_product_record
-- ----------------------------
DROP TABLE IF EXISTS `fac_product_record`;
CREATE TABLE `fac_product_record` (
  `trade_id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float(11,2) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `trade_date` datetime DEFAULT NULL,
  `trade_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`trade_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `fac_product_record_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `fac_product_mess` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`product_id`) REFER `assembly_line_system/fac_product_mes';

-- ----------------------------
-- Records of fac_product_record
-- ----------------------------
INSERT INTO `fac_product_record` VALUES ('1', '1.00', '94', '2014-11-04 08:40:15', '1');
INSERT INTO `fac_product_record` VALUES ('2', '1.00', '94', '2014-11-04 08:41:01', '1');
INSERT INTO `fac_product_record` VALUES ('3', '1.00', '94', '2014-11-04 08:42:18', '1');
INSERT INTO `fac_product_record` VALUES ('4', '1200.00', '230', '2014-11-07 18:32:01', '200');
INSERT INTO `fac_product_record` VALUES ('5', '600.00', '230', '2014-11-09 12:26:49', '100');
INSERT INTO `fac_product_record` VALUES ('6', '600.00', '230', '2014-11-09 12:27:01', '100');
INSERT INTO `fac_product_record` VALUES ('7', '600.00', '230', '2014-11-09 12:29:51', '100');

-- ----------------------------
-- Table structure for fac_product_transfer
-- ----------------------------
DROP TABLE IF EXISTS `fac_product_transfer`;
CREATE TABLE `fac_product_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_product_transfer
-- ----------------------------

-- ----------------------------
-- Table structure for fac_user_department
-- ----------------------------
DROP TABLE IF EXISTS `fac_user_department`;
CREATE TABLE `fac_user_department` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`department_id`),
  KEY `department_id` (`department_id`) USING BTREE,
  CONSTRAINT `fac_user_department_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `fac_users` (`user_id`),
  CONSTRAINT `fac_user_department_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `fac_department` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of fac_user_department
-- ----------------------------
INSERT INTO `fac_user_department` VALUES ('90', '45');
INSERT INTO `fac_user_department` VALUES ('98', '45');
INSERT INTO `fac_user_department` VALUES ('106', '53');
INSERT INTO `fac_user_department` VALUES ('92', '55');
INSERT INTO `fac_user_department` VALUES ('101', '57');
INSERT INTO `fac_user_department` VALUES ('102', '58');
INSERT INTO `fac_user_department` VALUES ('103', '59');
INSERT INTO `fac_user_department` VALUES ('105', '61');
INSERT INTO `fac_user_department` VALUES ('107', '62');
INSERT INTO `fac_user_department` VALUES ('111', '62');
INSERT INTO `fac_user_department` VALUES ('108', '63');
INSERT INTO `fac_user_department` VALUES ('109', '64');
INSERT INTO `fac_user_department` VALUES ('110', '65');

-- ----------------------------
-- Table structure for fac_user_type
-- ----------------------------
DROP TABLE IF EXISTS `fac_user_type`;
CREATE TABLE `fac_user_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fac_user_type
-- ----------------------------
INSERT INTO `fac_user_type` VALUES ('1', '厂长');
INSERT INTO `fac_user_type` VALUES ('2', '业务部');
INSERT INTO `fac_user_type` VALUES ('3', '采购部');
INSERT INTO `fac_user_type` VALUES ('4', '车间管理员');
INSERT INTO `fac_user_type` VALUES ('5', '超级管理员');

-- ----------------------------
-- Table structure for fac_users
-- ----------------------------
DROP TABLE IF EXISTS `fac_users`;
CREATE TABLE `fac_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `user_password` varchar(512) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `user_type_id` (`user_type_id`) USING BTREE,
  CONSTRAINT `fac_users_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `fac_user_type` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 3072 kB; (`user_type_id`) REFER `assembly_line_system/fac_user_type';

-- ----------------------------
-- Records of fac_users
-- ----------------------------
INSERT INTO `fac_users` VALUES ('81', '厂长', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '1');
INSERT INTO `fac_users` VALUES ('90', '抛光车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('91', '老板', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '5');
INSERT INTO `fac_users` VALUES ('92', 'CNC车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('98', '开料车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('101', '扫光车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('102', '钢化前车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('103', '钢化后车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('105', '成品车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('106', '贴合车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('107', '钢化前超声波', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('108', '钢化后质检', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('109', '指纹油', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('110', '成品车间', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
INSERT INTO `fac_users` VALUES ('111', '扫光后超声波', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4');
